<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Bantu 
{
	public static function getStatus($status)
	{	
		if($status == 1)
			return "Ya";
		else
			return "Bukan";
	}
	
	public static function m2($data)
	{
		return $data.' M<sup>2</sup>';
	}
	
	public static function rp($jumlah=null)
	{
		return 'Rp '.number_format($jumlah,0,',','.');
	}
	
	public static function tanggal($tgl=null)
	{
		if($tgl == '0000-00-00' OR $tgl == null)
			return null;
		else
			return date('j',strtotime($tgl))." ".Bantu::getBulan($tgl)." ".date('Y',strtotime($tgl));	
	}
	
	public static function getBulan($tgl)
	{
		$bulan=date('n',strtotime($tgl));
		
		if($bulan==1) return "Januari";
		if($bulan==2) return "Februari";
		if($bulan==3) return "Maret";
		if($bulan==4) return "April";
		if($bulan==5) return "Mei";
		if($bulan==6) return "Juni";
		if($bulan==7) return "Juli";
		if($bulan==8) return "Agustus";
		if($bulan==9) return "September";
		if($bulan==10) return "Oktober";
		if($bulan==11) return "November";
		if($bulan==12) return "Desember";
	}
	
	public static function getTerbilang($rp,$tri)
	{
		$ones = array(
			"",
			" Satu",
			" Dua",
			" Tiga",
			" Empat",
			" Lima",
			" Enam",
			" Tujuh",
			" Delapan",
			" Sembilan",
			" SePuluh",
			" Sebelas",
			" Dua Belas",
			" Tiga Belas",
			" Empat Belas",
			" Lima Belas",
			" Enam Belas",
			" Tujuh Belas",
			" Delapan Belas",
			" Sembilan Belas"
		);

		$tens = array(
			"",
			"",
			" Dua Puluh",
			" Tiga Puluh",
			" Empat Puluh",
			" Lima Puluh",
			" Enam Puluh",
			" Tujuh Puluh",
			" Delapan Puluh",
			" Sembilan Puluh"
		);

		$triplets = array(
			"",
			" Ribu",
			" Juta",
			" Miliar",
			" Triliun",
		);

		// chunk the number, ...rxyy
		$r = (int) ($rp / 1000);
		$x = ($rp / 100) % 10;
		$y = $rp % 100;

		// init the output string
		$str = "";

		// do hundreds
		if ($x > 0)
		{
			if($x==1)	
				$str =  "Seratus";
			else
				$str = $ones[$x] . " Ratus";
		}
		
		// do ones and tens
		if ($y < 20)
			$str .= $ones[$y];
		else
			$str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

		// add triplet modifier only if there
		// is some output to be modified...
		if ($str != "")
			$str .= $triplets[$tri];

		// continue recursing?
		if ($r > 0)
			return Bantu::getTerbilang($r, $tri+1).$str;
		else
			return $str;
	}
}