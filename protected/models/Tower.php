<?php

/**
 * This is the model class for table "tower".
 *
 * The followings are the available columns in table 'tower':
 * @property integer $id
 * @property integer $id_operator
 * @property integer $id_pemilik
 * @property integer $id_penyewa
 * @property string $alamat
 * @property integer $id_kecamatan
 * @property string $luas_lahan
 * @property string $koordinat
 */
class Tower extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tower';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_operator, id_pemilik, id_penyewa, alamat, id_kecamatan, luas_lahan, koordinat', 'required'),
			array('id_pemilik, id_kecamatan', 'numerical', 'integerOnly'=>true),
			array('njop, habis_izin, jumlah_operator,id_desa, jumlah_pengawasan, kondisi_sekitar', 'safe'),
			array('id_operator, nomor_izin, id_penyewa, luas_lahan, koordinat', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_operator, id_pemilik, id_penyewa, alamat, id_kecamatan, luas_lahan, koordinat, njop, jumlah_operator, jumlah_pengawasan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Operator'=>array(self::BELONGS_TO,'Perusahaan','id_operator'),
			'Pemilik'=>array(self::BELONGS_TO,'Perusahaan','id_pemilik'),
			'Penyewa'=>array(self::BELONGS_TO,'Perusahaan','id_penyewa'),
			'Kecamatan'=>array(self::BELONGS_TO,'Kecamatan','id_kecamatan'),
			'Desa'=>array(self::BELONGS_TO,'Desa','id_desa'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_operator' => 'Operator',
			'id_pemilik' => 'Pemilik',
			'id_penyewa' => 'Penyewa',
			'alamat' => 'Alamat',
			'id_kecamatan' => 'Kecamatan',
			'luas_lahan' => 'Luas Lahan',
			'koordinat' => 'Koordinat',
			'njop' => 'NJOP',
			'jumlah_operator' => 'Jumlah Operator',
			'jumlah_pengawasan' => 'Jumlah Pengawasan',
			'id_desa' => 'Desa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_operator',$this->id_operator,true);
		$criteria->compare('id_pemilik',$this->id_pemilik);
		$criteria->compare('id_penyewa',$this->id_penyewa,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('id_kecamatan',$this->id_kecamatan);
		$criteria->compare('luas_lahan',$this->luas_lahan,true);
		$criteria->compare('koordinat',$this->koordinat,true);
		$criteria->compare('njop',$this->njop,true);
		$criteria->compare('jumlah_operator',$this->jumlah_operator,true);
		$criteria->compare('jumlah_pengawasan',$this->jumlah_pengawasan,true);
		$criteria->compare('id_desa',$this->id_desa,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tower the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
	
	public function getDetailMarker($id_tower)
	{
		$data = Tower::model()->findByPk($id_tower);
		$return = '<div class="row">
			<div class="col-xs-12 table-responsive">
			<h4 class="text-center">Detail Tower</h4>
			<table class="table table-hover table-striped table-condensed">
			<tbody>
				<tr>
					<td>Pemilik</td>
					<td>'.$data->getRelationField('Pemilik','nama').'</td>
				</tr>
				<tr>
					<td>Penyewa</td>
					<td>'.$data->id_penyewa.'</td>
				</tr>
				<tr>
					<td>Operator</td>
					<td>'.$data->id_operator.'</td>
				</tr>
				
				<tr>
					<td>Alamat</td>
					<td>'.$data->alamat.'</td>
				</tr>
				<tr>
					<td>Kecamatan</td>
					<td>'.$data->getRelationField('Kecamatan','kecamatan').'</td>
				</tr>
				<tr>
					<td>Desa</td>
					<td>'.$data->getRelationField('Desa','nama').'</td>
				</tr>				
				<tr>
					<td>Luas Lahan</td>
					<td>'.$data->luas_lahan.' M<sup>2</sup></td>
				</tr>
				<tr>
					<td>Koordinat</td>
					<td>'.$data->koordinat.'</td>
				</tr>
			</tbody>
			</table></div></div>';
			$return .= $data->getAllPhoto($data->id);
		return $return;
	}
	
	public function getAllPhoto($id_tower)
	{
		$model = Gambar::model()->findAllByAttributes(array('id_tower'=>$id_tower));
		
		$return  = '<div class="row"><div class="col-xs-12"><h4 class="text-center">Foto Lokasi Tower</h4></div>';
		foreach($model as $data) 
		{
			$return .= '<div class="col-xs-3">'.CHtml::image(Yii::app()->baseUrl.'/uploads/gambar/'.$data->file,'',array('class'=>'img-responsive')).'</div>';
		}
		$return .= '</div>';
		
		return $return;
	}
	
	public function getChart($type)
	{
		$list = array();
		
		$model = Tower::model()->findAll();
		
		if($type=='bar-operator') 
		{
			$perusahaan = Perusahaan::model()->findAllByAttributes(array('operator'=>1));
			foreach($perusahaan as $data)
			{
				$count = count(Tower::model()->findAllByAttributes(array('id_operator'=>$data->id)));
				$list[] = array('type'=>'column','name'=>$data->nama,'data'=>array((int)$count));
			}
		}
		if($type=='pie-operator')
		{
			$perusahaan = Perusahaan::model()->findAllByAttributes(array('operator'=>1));
			foreach($perusahaan as $data)
			{
				$count = Tower::model()->countByAttributes(array('id_operator'=>$data->id));
				$list[] = array($data->nama,(int)$count);
			}
		}
		if($type=='bar-pemilik')
		{
			$perusahaan = Perusahaan::model()->findAllByAttributes(array('pemilik'=>1));
			foreach($perusahaan as $data)
			{
				$count = count(Tower::model()->findAllByAttributes(array('id_pemilik'=>$data->id)));
				$list[] = array('type'=>'column','name'=>$data->nama,'data'=>array((int)$count));
			}
		}
		if($type=='pie-pemilik')
		{
			$perusahaan = Perusahaan::model()->findAllByAttributes(array('pemilik'=>1));
			foreach($perusahaan as $data)
			{
				$count = Tower::model()->countByAttributes(array('id_pemilik'=>$data->id));
				$list[] = array($data->nama,(int)$count);
			}
		}
		if($type=='bar-penyewa')
		{
			$perusahaan = Perusahaan::model()->findAllByAttributes(array('penyewa'=>1));
			foreach($perusahaan as $data)
			{
				$count = count(Tower::model()->findAllByAttributes(array('id_penyewa'=>$data->id)));
				$list[] = array('type'=>'column','name'=>$data->nama,'data'=>array((int)$count));
			}
		}
		if($type=='pie-penyewa')
		{
			$perusahaan = Perusahaan::model()->findAllByAttributes(array('penyewa'=>1));
			foreach($perusahaan as $data)
			{
				$count = Tower::model()->countByAttributes(array('id_penyewa'=>$data->id));
				$list[] = array($data->nama,(int)$count);
			}
		}
		
		if($type=='bar-kecamatan')
		{
			$kecamatan = Kecamatan::model()->findAll();
			foreach($kecamatan as $data)
			{
				$count = count(Tower::model()->findAllByAttributes(array('id_kecamatan'=>$data->id)));
				$list[] = array('type'=>'column','name'=>$data->kecamatan,'data'=>array((int)$count));
			}
		}
		if($type=='pie-kecamatan')
		{
			$kecamatan = Kecamatan::model()->findAll();
			foreach($kecamatan as $data)
			{
				$count = Tower::model()->countByAttributes(array('id_kecamatan'=>$data->id));
				$list[] = array($data->kecamatan,(int)$count);
			}
		}
		
		return $list;
	}

	public function findAllRetribusi()
	{
		$model = Retribusi::model()->findAllByAttributes(array('id_tower'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}

	public function getDesa()
	{
		$model = Desa::model()->findByPk($this->id_desa);
		if($model!==null)
			return $model->nama;
		else
			return null;
	}
	
	public function getData()
	{
		$model = Perusahaan::model()->findAll();
		foreach($model as $data)
		{
			$tags[] = $data->nama;
		}
		return $tags;
	}

	public function getListOperator()
	{
		$model = Operator::model()->findAll();
		$tags = array();
		foreach($model as $data)
		{
			$tags[] = $data->nama;
		}
		return $tags;
	}

}
