<?php

/**
 * This is the model class for table "pengaduan".
 *
 * The followings are the available columns in table 'pengaduan':
 * @property integer $id
 * @property string $kode
 * @property string $nama
 * @property string $email
 * @property string $telepon
 * @property integer $id_kategori
 * @property string $keluhan
 * @property integer $id_status
 * @property string $waktu_dibuat
 */
class Pengaduan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pengaduan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode, nama, email, telepon, id_pengaduan_kategori, keluhan, id_pengaduan_status', 'required','message'=>'{attribute} tidak boleh kosong'),
			array('id_pengaduan_kategori, id_pengaduan_status', 'numerical', 'integerOnly'=>true),
			array('kode, nama, alamat, email, telepon', 'length', 'max'=>255),
			array('email','email','message'=>'Alamat email anda tidak benar'),
			array('waktu_dibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, kode, nama, email, telepon, id_kategori, keluhan, id_status, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'kode' => 'Kode',
			'nama' => 'Nama',
			'email' => 'Email',
			'telepon' => 'Telepon',
			'id_pengaduan_kategori' => 'Kategori',
			'keluhan' => 'Keluhan',
			'id_status' => 'Id Status',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('id_pengaduan_kategori',$this->id_pengaduan_kategori);
		$criteria->compare('keluhan',$this->keluhan,true);
		$criteria->compare('id_pengaduan_status',$this->id_pengaduan_status);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		$criteria->order = 'waktu_dibuat DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pengaduan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getKategori()
	{
		$model = PengaduanKategori::model()->findByPk($this->id_pengaduan_kategori);
		if ($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getStatus()
	{
		$model = PengaduanStatus::model()->findByPk($this->id_pengaduan_status);
		if ($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getCountData()
	{
		$result = $this->findAll();

		$count = count($result);
		return $count;
	}

	public function findAllTanggapan()
	{
		$model = Tanggapan::model()->findAllByAttributes(array('id_pengaduan'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}

		public function findAllPengaduan() 
	{
		$model = $this->findAll();
		if($model!==null)
			return $model;
		else
			return false;
	}

	public static function getDataChartBulan()
	{
		$dataChartBulan = '';

		for($i=1;$i<=12;$i++)
		{
   		 $criteria = new CDbCriteria;
    
    		$bulan = $i;

    		if($i<=10) $bulan = '0'.$i;

   			 $awal = date('Y').'-'.$bulan.'-01';
    		$akhir = date('Y').'-'.$bulan.'-31';
    
	    $criteria->condition = 'waktu_dibuat >= :awal AND waktu_dibuat <= :akhir';
	    $criteria->params = array(':awal'=>$awal,':akhir'=>$akhir);
    
	    $jumlah_acara = Pengaduan::model()->count($criteria);

	    $nama_bulan = '';
	    if($i==1) $nama_bulan = 'Jan';
	    if($i==2) $nama_bulan = 'Feb';
	    if($i==3) $nama_bulan = 'Mar';
	    if($i==4) $nama_bulan = 'Apr';
	    if($i==5) $nama_bulan = 'Mei';
	    if($i==6) $nama_bulan = 'Jun';
	    if($i==7) $nama_bulan = 'Jul';
	    if($i==8) $nama_bulan = 'Aug';
	    if($i==9) $nama_bulan = 'Sep';
	    if($i==10) $nama_bulan = 'Okt';
	    if($i==11) $nama_bulan = 'Nov';
	    if($i==12) $nama_bulan = 'Des';


    
    $dataChartBulan .= '{"label":"'.$nama_bulan.'","value":"'.$jumlah_acara.'"},';
	}
		return $dataChartBulan;

	}


	public static function getdataChartKategori()
	{

		$dataChartJenis = '';

 		foreach(PengaduanKategori::model()->findAll() as $data) 
  		{ 
			$dataChartJenis .= '{"label":"'.$data->nama.'","value":"'.$data->getCountData().'"},';
  		}
  			return $dataChartJenis;
	}



}
