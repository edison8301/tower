<?php

/**
 * This is the model class for table "tanggapan".
 *
 * The followings are the available columns in table 'tanggapan':
 * @property integer $id
 * @property integer $id_pengaduan
 * @property string $tanggapan
 * @property integer $pengirim
 * @property string $waktu_dibuat
 */
class Tanggapan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tanggapan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pengaduan, tanggapan, pengirim, waktu_dibuat', 'required'),
			array('id_pengaduan', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_pengaduan, tanggapan, pengirim, waktu_dibuat, waktu_dilihat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_pengaduan' => 'Id Pengaduan',
			'tanggapan' => 'Tanggapan',
			'pengirim' => 'Pengirim',
			'waktu_dilihat' => 'Waktu Dilihat',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_pengaduan',$this->id_pengaduan);
		$criteria->compare('tanggapan',$this->tanggapan,true);
		$criteria->compare('pengirim',$this->pengirim);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tanggapan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

			public function getPengaduan()
	{
		$model = Pengaduan::model()->findByPk($this->id_pengaduan);
		if ($model!==null)
			return $model->nama;
		else
			return null;
	}
}
