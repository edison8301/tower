<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class Search extends CFormModel
{
	public $operator;
	public $pemilik;
	public $penyewa;
	public $kecamatan;
	public $desa;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			//array('field, query', 'required'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'operator'=>'',
			'penyewa'=>'',
			'kecamatan'=>'',
			'pemilik'=>'',
			'desa'=>'',
		);
	}
}