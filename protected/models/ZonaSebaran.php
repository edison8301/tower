<?php

/**
 * This is the model class for table "zona_sebaran".
 *
 * The followings are the available columns in table 'zona_sebaran':
 * @property integer $id
 * @property string $no_site
 * @property integer $id_kecamatan
 * @property integer $id_desa
 * @property string $longitude
 * @property string $lattitude
 */
class ZonaSebaran extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'zona_sebaran';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_site, id_kecamatan', 'required'),
			array('id_kecamatan, id_desa', 'numerical', 'integerOnly'=>true),
			array('no_site', 'length', 'max'=>255),
			array('longitude, lattitude', 'length', 'max'=>12),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, no_site, id_kecamatan, id_desa, longitude, lattitude', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'no_site' => 'No Site',
			'id_kecamatan' => 'Kecamatan',
			'id_desa' => 'Desa',
			'longitude' => 'Longitude',
			'lattitude' => 'Lattitude',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('no_site',$this->no_site,true);
		$criteria->compare('id_kecamatan',$this->id_kecamatan);
		$criteria->compare('id_desa',$this->id_desa);
		$criteria->compare('longitude',$this->longitude,true);
		$criteria->compare('lattitude',$this->lattitude,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ZonaSebaran the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getKecamatan()
	{
		$model = Kecamatan::model()->findByPk($this->id_kecamatan);
		if ($model !==null)
			return $model->kecamatan;
		else
			return null;
	}

	public function getDesa()
	{
		$model = Desa::model()->findByPk($this->id_desa);
		if ($model !==null)
			return $model->nama;
		else
			return null;
	}
}
