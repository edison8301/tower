<?php

/**
 * This is the model class for table "retribusi".
 *
 * The followings are the available columns in table 'retribusi':
 * @property integer $id
 * @property integer $id_perusahaan
 * @property integer $id_tower
 * @property string $nomor
 * @property string $nama_perusahaan
 * @property string $alamat_perusahaan
 * @property string $peruntukan
 * @property string $lokasi
 * @property string $luas_lahan
 * @property string $njop
 * @property string $persentase
 * @property integer $jumlah_operator
 * @property integer $jumlah_pengawasan
 * @property string $jatuh_tempo
 * @property string $tanggal_dibuat
 */
class Retribusi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'retribusi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tower, nomor, tanggal, jumlah', 'required','message'=>'{attribute} harus diisi'),
			array('id_perusahaan, id_tower, jumlah, jumlah_operator, jumlah_pengawasan', 'numerical', 'integerOnly'=>true),
			array('nomor, nama_perusahaan, peruntukan, luas_lahan, njop, persentase', 'length', 'max'=>255),
			array('alamat_perusahaan, tanggal, tanggal_dibuat, jatuh_tempo', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_perusahaan, id_tower, nomor, nama_perusahaan, alamat_perusahaan, peruntukan, lokasi, luas_lahan, njop, persentase, jumlah_operator, jumlah_pengawasan, jatuh_tempo, tanggal_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Tower'=>array(self::BELONGS_TO,'Tower','id_tower'),
			'Perusahaan'=>array(self::BELONGS_TO,'Perusahaan','id_perusahaan'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_perusahaan' => 'Perusahaan',
			'id_tower' => 'Tower',
			'nomor' => 'Nomor',
			'nama_perusahaan' => 'Nama Perusahaan',
			'alamat_perusahaan' => 'Alamat Perusahaan',
			'peruntukan' => 'Peruntukan',
			'lokasi' => 'Lokasi',
			'luas_lahan' => 'Luas Lahan',
			'njop' => 'Njop',
			'persentase' => 'Persentase',
			'jumlah_operator' => 'Jumlah Operator',
			'jumlah_pengawasan' => 'Jumlah Pengawasan',
			'jatuh_tempo' => 'Jatuh Tempo',
			'tanggal_dibuat' => 'Tanggal Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_perusahaan',$this->id_perusahaan);
		$criteria->compare('id_tower',$this->id_tower);
		$criteria->compare('nomor',$this->nomor,true);
		$criteria->compare('nama_perusahaan',$this->nama_perusahaan,true);
		$criteria->compare('alamat_perusahaan',$this->alamat_perusahaan,true);
		$criteria->compare('peruntukan',$this->peruntukan,true);
		$criteria->compare('lokasi',$this->lokasi,true);
		$criteria->compare('luas_lahan',$this->luas_lahan,true);
		$criteria->compare('njop',$this->njop,true);
		$criteria->compare('persentase',$this->persentase,true);
		$criteria->compare('jumlah_operator',$this->jumlah_operator);
		$criteria->compare('jumlah_pengawasan',$this->jumlah_pengawasan);
		$criteria->compare('jatuh_tempo',$this->jatuh_tempo,true);
		$criteria->compare('tanggal_dibuat',$this->tanggal_dibuat,true);

		if(!User::isAdmin())
		{
			$criteria->compare('id_perusahaan',Perusahaan::getIdByUserId());
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));


	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Retribusi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function listData()
	{
		
		$models = Tower::model()->findAll();
		
		foreach($models as $model)
		{
			$listData[$model->id]=$model->getRelationField('Operator','nama').' | '.$model->alamat;
		}
		return $listData;
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}

	public function setJatuhTempo()
	{
	
		$date = strtotime(date("Y-m-d", strtotime($this->tanggal)) . " +1 month");
		$this->jatuh_tempo =  date('Y-m-d', $date);
	}

	public function getDenda()
	{
		return null;
	}

	public function getTotal()
	{
		return $this->jumlah + $this->getDenda();
	}
}
