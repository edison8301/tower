<h1>Kelola Gambar</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'gambar-grid',
	'type'=>'striped bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_tower',
		'file',
		array(
			'class'=>'booster.widgets.TbButtonColumn',
		),
	),
)); ?>
