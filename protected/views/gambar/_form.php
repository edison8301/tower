<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'gambar-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
	
	<?php if(isset($_GET['id_tower']) AND $_GET['id_tower']!=null) { ?>
		<?php echo $form->hiddenField($model,'id_tower',array('value'=>$_GET['id_tower'])); ?>
	<?php } else { ?>
		<?php echo $form->dropDownListGroup($model,'id_tower',array('widgetOptions'=>array('data'=>CHtml::listData(Tower::model()->findAll(),'id','koordinat'),'htmlOptions'=>array('empty'=>'-- Select Parent --')))); ?>
	<?php } ?>
	
	<?php if(!empty($model->file)) print CHtml::image(Yii::app()->request->baseUrl.'/uploads/gambar/'.$model->file); ?>
	<?php echo $form->fileFieldGroup($model,'file',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php  echo $form->numberFieldGroup($model,'tahun',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255,'value'=>date('Y')))));  ?>
	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>

<script>
	$(document).ready(function() {
    $("#Gambar_tahun").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	});
</script>