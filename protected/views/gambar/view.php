<h1>Lihat Gambar</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
	array(
		'label'=>'Thumbnail',
		'type'=>'raw',
		'value'=>$model->file == '' ? '' : CHtml::image(Yii::app()->request->baseUrl.'/uploads/gambar/'.$model->file,'',array('width'=>'600px'))
	),
),
)); ?>
