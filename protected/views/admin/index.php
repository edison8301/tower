<h1><i>Rekapitulasi Sebaran Menara Di Kabupaten Serang</i></h1>

<?php $this->beginWidget('booster.widgets.TbPanel', array(
		'title' => 'Sebaran Menara Berdasarkan Operator',
		'context'=>'primary',
		'headerIcon' => 'list',
)); ?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'title' => array(
					'text' => 'Sebaran Menara Berdasarkan Operator',
				),
				'xAxis' => array(
					'categories' => ['Operator']
				),
				'yAxis' => array(
					'title' => array(
						'text' => 'Total',
					),
				),
				'series' => Tower::model()->getChart('bar-operator')
			),
		));?>
	</div>
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'theme' => '', 
				'title' => array(
					'text' => 'Sebaran Menara Berdasarkan Operator'
				),
				'series' => array(
					array(
						'type' => 'pie',
						'name' => 'Sebaran Menara Berdasarkan Operator',
						'data' => Tower::model()->getChart('pie-operator')
					),
				),
			)
		));?>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th class="text-center">Nama Operator</th>
						<th class="text-center">Total Menara</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach(Perusahaan::model()->findAllByAttributes(array('operator'=>1)) as $data) { ?>
					<tr>
						<td><?php print $data->nama; ?></td>
						<td class="text-center"><span class="badge"><?php print Tower::model()->countByAttributes(array('id_operator'=>$data->id)); ?></span></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<div>&nbsp;</div>

<?php $this->beginWidget('booster.widgets.TbPanel', array(
		'title' => 'Sebaran Menara Berdasarkan Pemilik',
		'headerIcon' => 'list',
)); ?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'title' => array(
					'text' => 'Sebaran Menara Berdasarkan Pemilik',
				),
				'xAxis' => array(
					'categories' => ['Pemilik Tower']
				),
				'yAxis' => array(
					'title' => array(
						'text' => 'Total',
					),
				),
				'series' => Tower::model()->getChart('bar-pemilik')
			),
		));?>
	</div>
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'theme' => '', 
				'title' => array(
					'text' => 'Sebaran Menara Berdasarkan Pemilik'
				),
				'series' => array(
					array(
						'type' => 'pie',
						'name' => 'Sebaran Menara Berdasarkan Pemilik',
						'data' => Tower::model()->getChart('pie-pemilik')
					),
				),
			)
		));?>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th class="text-center">Nama Pemilik</th>
						<th class="text-center">Total Menara</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach(Perusahaan::model()->findAllByAttributes(array('pemilik'=>1)) as $data) { ?>
					<tr>
						<td><?php print $data->nama; ?></td>
						<td class="text-center"><span class="badge"><?php print Tower::model()->countByAttributes(array('id_pemilik'=>$data->id)); ?></span></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<div>&nbsp;</div>

<?php $this->beginWidget('booster.widgets.TbPanel', array(
		'title' => 'Sebaran Menara Berdasarkan Penyewa',
		'headerIcon' => 'list',
)); ?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'title' => array(
					'text' => 'Sebaran Menara Berdasarkan Penyewa',
				),
				'xAxis' => array(
					'categories' => ['Penyewa Tower']
				),
				'yAxis' => array(
					'title' => array(
						'text' => 'Total',
					),
				),
				'series' => Tower::model()->getChart('bar-penyewa')
			),
		));?>
	</div>
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'theme' => '', 
				'title' => array(
					'text' => 'Sebaran Menara Berdasarkan Penyewa'
				),
				'series' => array(
					array(
						'type' => 'pie',
						'name' => 'Sebaran Menara Berdasarkan Penyewa',
						'data' => Tower::model()->getChart('pie-penyewa')
					),
				),
			)
		));?>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th class="text-center">Nama Penyewa</th>
						<th class="text-center">Total Menara</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach(Perusahaan::model()->findAllByAttributes(array('penyewa'=>1)) as $data) { ?>
					<tr>
						<td><?php print $data->nama; ?></td>
						<td class="text-center"><span class="badge"><?php print Tower::model()->countByAttributes(array('id_penyewa'=>$data->id)); ?></span></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<div>&nbsp;</div>

<?php $this->beginWidget('booster.widgets.TbPanel', array(
		'title' => 'Sebaran Menara Berdasarkan Kecamatan',
		'headerIcon' => 'list',
)); ?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'title' => array(
					'text' => 'Sebaran Menara Berdasarkan kecamatan',
				),
				'xAxis' => array(
					'categories' => ['Kecamatan']
				),
				'yAxis' => array(
					'title' => array(
						'text' => 'Total',
					),
				),
				'series' => Tower::model()->getChart('bar-kecamatan')
			),
		));?>
	</div>
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'theme' => '', 
				'title' => array(
					'text' => 'Sebaran Menara Berdasarkan Kecamatan'
				),
				'series' => array(
					array(
						'type' => 'pie',
						'name' => 'Sebaran Menara Berdasarkan Kecamatan',
						'data' => Tower::model()->getChart('pie-kecamatan')
					),
				),
			)
		));?>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th class="text-center">Nama Kecamatan</th>
						<th class="text-center">Total Menara</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach(Kecamatan::model()->findAll() as $data) { ?>
					<tr>
						<td><?php print $data->kecamatan; ?></td>
						<td class="text-center"><span class="badge"><?php print Tower::model()->countByAttributes(array('id_kecamatan'=>$data->id)); ?></span></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>