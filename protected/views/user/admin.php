<h1>Kelola User</h1>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'user-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			'username',
			array(
				'name'=>'id_role',
				'header'=>'Role',
				'value'=>'$data->getRelationField("Role","name")',
				'filter'=>CHtml::listData(Role::model()->findAll(array('order'=>'name ASC')),'id','name'),
				'type'=>'raw'
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
<?php $this->widget('booster.widgets.TbButton', array(
	'buttonType'=>'link',
	'url'=>array('create'),
	'context'=>'primary',
	'icon'=>'plus',
	'size'=>'small',
	'label'=>'Tambah User',
)); ?>
</div>