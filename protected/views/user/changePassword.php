

<h1>Change Password</h1>

<div>&nbsp;</div>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
		'id' => 'chnage-password-form',
		'type'=>'horizontal',
        'enableClientValidation' => true,
        'clientOptions' => array(
        	'validateOnSubmit' => true,
        ),
)); ?>
	
	<div class="well">
	<?php echo $form->passwordFieldGroup($model,'old_password',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	<?php echo $form->passwordFieldGroup($model,'new_password',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	<?php echo $form->passwordFieldGroup($model,'repeat_password',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-5'),
			'widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?><div>&nbsp;</div>
	</div>

	<div class="form-actions well">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType' => 'submit', 
				'context' => 'primary', 
				'label' => 'Change Password',
				'icon' => 'ok'
		)); ?>
	</div>
	
<?php $this->endWidget(); ?>