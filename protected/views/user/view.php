<h1>Detail User</h1>

<?php $this->widget('booster.widgets.TbButton', array(
	'buttonType'=>'link',
	'url'=>array('update','id'=>$model->id),
	'context'=>'primary',
	'icon'=>'pencil',
	'label'=>'Sunting User',
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton', array(
	'buttonType'=>'link',
	'url'=>array('create'),
	'context'=>'primary',
	'icon'=>'plus',
	'label'=>'Tambah User',
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton', array(
	'buttonType'=>'link',
	'url'=>array('admin'),
	'context'=>'primary',
	'icon'=>'list',
	'label'=>'Kelola User',
)); ?>&nbsp;

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'username',
		array(
			'label'=>'Role',
			'value'=>$model->Role->name
		)
),
)); ?>
