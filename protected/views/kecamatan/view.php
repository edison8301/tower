<?php
$this->breadcrumbs=array(
	'Kecamatans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Kecamatan','url'=>array('index')),
array('label'=>'Create Kecamatan','url'=>array('create')),
array('label'=>'Update Kecamatan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Kecamatan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Kecamatan','url'=>array('admin')),
);
?>

<h1>Lihat Kecamatan</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting Kecamatan',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola Kecamatan',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('admin')
)); ?>&nbsp;

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'kecamatan',
),
)); ?>
