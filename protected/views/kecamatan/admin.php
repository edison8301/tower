<h1>Kelola Kecamatan</h1>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'kecamatan-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			'kecamatan',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Kecamatan',
		'icon'=>'plus',
		'size'=>'small',
		'context'=>'primary',
		'url'=>array('create')
	)); ?>&nbsp;
</div>