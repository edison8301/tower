<h1>Kelola Roles</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'role-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'name',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
