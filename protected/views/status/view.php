<?php
$this->breadcrumbs=array(
	'Statuses'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Status','url'=>array('index')),
array('label'=>'Create Status','url'=>array('create')),
array('label'=>'Update Status','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Status','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Status','url'=>array('admin')),
);
?>

<h1>Status <b><?php echo $model->nama; ?></b></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('status/update','id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
