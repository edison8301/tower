<?php
$this->breadcrumbs=array(
	'Statuses'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Status','url'=>array('index')),
array('label'=>'Create Status','url'=>array('create')),
);

?>

<h1>Kelola Status</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('status/create')
)); ?>&nbsp;


<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'status-grid',
'dataProvider'=>$model->search(),
'type' => 'striped bordered',
'filter'=>$model,
'columns'=>array(
		'id',
		'nama',
array(
'class'=>'booster.widgets.TbButtonColumn',
'htmlOptions' => array('style' => 'width: 80px;'),
),
),
)); ?>
