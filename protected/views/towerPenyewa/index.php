<?php
/* @var $this TowerPenyewaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tower Penyewas',
);

$this->menu=array(
	array('label'=>'Create TowerPenyewa', 'url'=>array('create')),
	array('label'=>'Manage TowerPenyewa', 'url'=>array('admin')),
);
?>

<h1>Tower Penyewas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
