<?php
/* @var $this TowerPenyewaController */
/* @var $model TowerPenyewa */

$this->breadcrumbs=array(
	'Tower Penyewas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TowerPenyewa', 'url'=>array('index')),
	array('label'=>'Manage TowerPenyewa', 'url'=>array('admin')),
);
?>

<h1>Create TowerPenyewa</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>