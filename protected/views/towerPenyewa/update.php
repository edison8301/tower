<?php
/* @var $this TowerPenyewaController */
/* @var $model TowerPenyewa */

$this->breadcrumbs=array(
	'Tower Penyewas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TowerPenyewa', 'url'=>array('index')),
	array('label'=>'Create TowerPenyewa', 'url'=>array('create')),
	array('label'=>'View TowerPenyewa', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TowerPenyewa', 'url'=>array('admin')),
);
?>

<h1>Update TowerPenyewa <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>