<?php
$this->breadcrumbs=array(
	'Zona Sebarans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ZonaSebaran','url'=>array('index')),
array('label'=>'Create ZonaSebaran','url'=>array('create')),
array('label'=>'Update ZonaSebaran','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ZonaSebaran','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ZonaSebaran','url'=>array('admin')),
);
?>

<h1>Lihat Zona Sebaran</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('zonaSebaran/update','id'=>$model->id),
)); ?>&nbsp;

<div> &nbsp; </div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' =>'striped bordered',
'attributes'=>array(
		'id',
		'no_site',
		array(
			'label' => 'Kecamatan',
			'value' => $model->getKecamatan()),
		array(
			'label' => 'Desa',
			'value' => $model->getDesa()),
		'longitude',
		'lattitude',
),
)); ?>
