<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'zona-sebaran-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'no_site',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'id_kecamatan',array('widgetOptions'=>array('data' => Chtml::ListData(Kecamatan::model()->findAll(),'id','kecamatan'),'htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->dropDownListGroup($model,'id_desa',array('widgetOptions'=>array('data' => Chtml::ListData(Desa::model()->findAll(),'id','nama'),'htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'longitude',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>12)))); ?>

	<?php echo $form->textFieldGroup($model,'lattitude',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>12)))); ?>

<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon' => 'ok',
			'label'=>'Add',
		)); ?>
</div>

<?php $this->endWidget(); ?>
