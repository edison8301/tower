<?php
$this->breadcrumbs=array(
	'Zona Sebarans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List ZonaSebaran','url'=>array('index')),
	array('label'=>'Create ZonaSebaran','url'=>array('create')),
	array('label'=>'View ZonaSebaran','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ZonaSebaran','url'=>array('admin')),
	);
	?>

	<h1>Sunting Zona Sebaran</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>