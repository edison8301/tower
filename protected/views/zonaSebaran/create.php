<?php
$this->breadcrumbs=array(
	'Zona Sebarans'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ZonaSebaran','url'=>array('index')),
array('label'=>'Manage ZonaSebaran','url'=>array('admin')),
);
?>

<h1>Tambah Zona Sebaran</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>