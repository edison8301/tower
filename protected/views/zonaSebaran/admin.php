<?php
$this->breadcrumbs=array(
	'Zona Sebarans'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Zona Sebaran</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Zona Sebaran',
		'icon'=>'plus',
		'context'=>'primary',
		'visible'=>User::isAdmin(),
		'url'=>array('create')
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'zona-sebaran-grid',
		'type' => 'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'no_site',
			array(
				'name' => 'id_kecamatan',
				'value' => '$data->getKecamatan()',
				'filter' => CHtml::ListData(Kecamatan::model()->findAll(),'id','kecamatan')
			),
			array(
				'name' => 'id_desa',
				'value' => '$data->getDesa()',
			'filter' => CHtml::ListData(Desa::model()->findAll(),'id','nama')
			),
		'longitude',
		'lattitude',
		array(
			'class'=>'booster.widgets.TbButtonColumn',
			'visible'=>User::isAdmin()
		),
	),
)); ?>
