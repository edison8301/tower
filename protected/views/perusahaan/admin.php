<h1>Kelola Perusahaan</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'perusahaan-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			'nama',
			array(
				'name'=>'pemilik',
				'header'=>'Pemilik',
				'type'=>'raw',
				'value'=>'Bantu::getStatus($data->pemilik)',
				'filter'=>array('1'=>'Ya','0'=>'Bukan'),
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'name'=>'penyewa',
				'header'=>'Penyewa',
				'type'=>'raw',
				'value'=>'Bantu::getStatus($data->penyewa)',
				'filter'=>array('1'=>'Ya','0'=>'Bukan'),
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'name'=>'operator',
				'header'=>'Operator',
				'type'=>'raw',
				'value'=>'Bantu::getStatus($data->operator)',
				'filter'=>array('1'=>'Ya','0'=>'Bukan'),
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'name'=>'username',
				'header'=>'Username',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
),
)); ?>

<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'size'=>'small',
		'label'=>'Tambah Perusahaan',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create')
	)); ?>&nbsp;

</dv>