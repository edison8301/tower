<div class="well">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'perusahaan-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<Div class="col-sm-6">
			<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textAreaGroup($model,'alamat', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>4, 'cols'=>50, 'class'=>'span8')))); ?>
		<?php echo $form->textFieldGroup($model,'username', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

	<?php echo $form->passwordFieldGroup($model,'password', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>


		</div>
		
		<Div class="col-sm-6">
	<?php echo $form->switchGroup($model,'pemilik',array(
		'widgetOptions' => array(
			'events'=>array(
				'switchChange'=>'js:function(event, state) {
				  console.log(this); // DOM element
				  console.log(event); // jQuery event
				  console.log(state); // true | false
				}'
			),
			'options'=>array(
				'onText'=>'Ya',
				'offText'=>'Bukan',
				'offColor'=>'danger',
				'onColor'=>'success',
			),
		)
	)); ?>

	<?php echo $form->switchGroup($model,'penyewa',array(
		'widgetOptions' => array(
			'events'=>array(
				'switchChange'=>'js:function(event, state) {
				  console.log(this); // DOM element
				  console.log(event); // jQuery event
				  console.log(state); // true | false
				}'
			),
			'options'=>array(
				'onText'=>'Ya',
				'offText'=>'Bukan',
				'offColor'=>'danger',
				'onColor'=>'success',
			),
		)
	)); ?>
	
	<?php echo $form->switchGroup($model,'operator',array(
		'widgetOptions' => array(
			'events'=>array(
				'switchChange'=>'js:function(event, state) {
				  console.log(this); // DOM element
				  console.log(event); // jQuery event
				  console.log(state); // true | false
				}'
			),
			'options'=>array(
				'onText'=>'Ya',
				'offText'=>'Bukan',
				'offColor'=>'danger',
				'onColor'=>'success',
			),
		)
	)); ?>

	</div>
</div>
	
	<hr>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Simpan',
			'icon'=>'ok',
		)); ?>
</div>

<?php $this->endWidget(); ?>
</div>
