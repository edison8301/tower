<h1>Detail Perusahaan</h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered condensed',
'attributes'=>array(
		'nama',
		'alamat',
		array(
			'label'=>'Pemilik',
			'value'=>Bantu::getStatus($model->pemilik)
		),
		array(
			'label'=>'Penyewa',
			'value'=>Bantu::getStatus($model->penyewa)
		),
		array(
			'label'=>'Operator',
			'value'=>Bantu::getStatus($model->operator)
		),
		'username'
),
)); ?>

<div>&nbsp;</div>

<div class="well">

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting Perusahaan',
		'icon'=>'pencil',
		'size'=>'small',
		'context'=>'primary',
		'url'=>array('update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola Perusahaan',
		'icon'=>'list',
		'size'=>'small',
		'context'=>'primary',
		'url'=>array('admin')
)); ?>&nbsp;


	</div>
