<h1>Detail Perusahaan</h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered condensed',
'attributes'=>array(
		'nama',
		'alamat',
		array(
			'label'=>'Pemilik',
			'value'=>Bantu::getStatus($model->pemilik)
		),
		array(
			'label'=>'Penyewa',
			'value'=>Bantu::getStatus($model->penyewa)
		),
		array(
			'label'=>'Operator',
			'value'=>Bantu::getStatus($model->operator)
		),
		'username'
),
)); ?>
