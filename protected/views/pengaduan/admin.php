<?php
$this->breadcrumbs=array(
	'Pengaduans'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Pengaduan','url'=>array('index')),
array('label'=>'Create Pengaduan','url'=>array('create')),
);
?>

<h1>Kelola Pengaduan</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'pengaduan-grid',
		'type' => 'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'kode',
			'nama',
			//'email',
			//'telepon',
			array(
			 	'header' => 'Kategori',
			 	'name' => 'id_pengaduan_kategori',
			 	'value' => '$data->getKategori()',
			 	'headerHtmlOptions' => array('width' =>'15%','style'=>'text-align:center'),
			 	'htmlOptions'=>array('style'=>'text-align:center'),
			 	'filter' => CHtml::listData(PengaduanKategori::model()->findAll(array('order'=>'nama ASC')),'id','nama')
			 ),
			array(
			 	'header' => 'Status',
			 	'name' => 'id_pengaduan_status',
			 	'value' => '$data->getStatus()',
			 	'headerHtmlOptions' => array('width' =>'15%','style'=>'text-align:center'),
			 	'htmlOptions'=>array('style'=>'text-align:center'),
			 	'filter' => CHtml::listData(PengaduanStatus::model()->findAll(array('order'=>'nama ASC')),'id','nama')
			 ),
			array(
				'name'=>'waktu_dibuat',
				'headerHtmlOptions' => array('width' =>'15%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'htmlOptions'=>array('style'=>'width:8%;text-align:center'),
			),
		),
)); ?>
