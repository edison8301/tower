<?php
$this->breadcrumbs=array(
	'Pengaduans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Pengaduan','url'=>array('index')),
array('label'=>'Create Pengaduan','url'=>array('create')),
array('label'=>'Update Pengaduan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Pengaduan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Pengaduan','url'=>array('admin')),
);
?>

<h1>Pengaduan <b><?php echo $model->nama; ?></b></h1>



<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'id',
		'kode',
		'nama',
		'email',
		'telepon',
		array(
			'label'=>'Kategori',
			'value'=>$model->getKategori()
			),
		'keluhan',
		array(
			'label'=>'Status',
			'value'=>$model->getStatus()
			),
		'waktu_dibuat',
),
));?> 

<div>&nbsp;</div> 

<div class="well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('pengaduan/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Beri Tanggapan',
		'icon'=>'share-alt',
		'context'=>'primary',
		'url'=>array('tanggapan/create','id_pengaduan'=>$model->id)
)); ?>
</div>

<?php $pengaduan = $_GET['id']; ?>

<div>&nbsp</div>

<h4>Tanggapan</h4>

<?php  $i=1; foreach($model->findAllTanggapan() as $tanggapan) { ?>
<div>&nbsp</div>
<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$tanggapan,
'type' => 'striped bordered',
'attributes'=>array(
		'pengirim',
		'waktu_dibuat',
		'tanggapan',
),
)); 
}
?>


