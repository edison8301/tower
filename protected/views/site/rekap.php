<h1><i>Rekapitulasi Sebaran Tower Di Kabupaten Serang</i></h1>

<?php $this->beginWidget('booster.widgets.TbPanel', array(
		'title' => 'Sebaran Tower Berdasarkan Operator',
		'headerIcon' => 'list',
)); ?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'title' => array(
					'text' => 'Sebaran Tower Berdasarkan Operator',
				),
				'xAxis' => array(
					'categories' => ['Operator']
				),
				'yAxis' => array(
					'title' => array(
						'text' => 'Total',
					),
				),
				'series' => Tower::model()->getChart('bar-operator')
			),
		));?>
	</div>
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'theme' => '', 
				'title' => array(
					'text' => 'Sebaran Tower Berdasarkan Operator'
				),
				'series' => array(
					array(
						'type' => 'pie',
						'name' => 'Sebaran Tower Berdasarkan Operator',
						'data' => Tower::model()->getChart('pie-operator')
					),
				),
			)
		));?>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th class="text-center">Nama Operator</th>
						<th class="text-center">Total Tower</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach(Perusahaan::model()->findAllByAttributes(array('operator'=>1)) as $data) { ?>
					<tr>
						<td><?php print $data->nama; ?></td>
						<td class="text-center"><span class="badge"><?php print Tower::model()->countByAttributes(array('id_operator'=>$data->id)); ?></span></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<div>&nbsp;</div>

<?php $this->beginWidget('booster.widgets.TbPanel', array(
		'title' => 'Sebaran Tower Berdasarkan Pemilik',
		'headerIcon' => 'list',
)); ?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'title' => array(
					'text' => 'Sebaran Tower Berdasarkan Pemilik',
				),
				'xAxis' => array(
					'categories' => ['Pemilik Tower']
				),
				'yAxis' => array(
					'title' => array(
						'text' => 'Total',
					),
				),
				'series' => Tower::model()->getChart('bar-pemilik')
			),
		));?>
	</div>
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'theme' => '', 
				'title' => array(
					'text' => 'Sebaran Tower Berdasarkan Pemilik'
				),
				'series' => array(
					array(
						'type' => 'pie',
						'name' => 'Sebaran Tower Berdasarkan Pemilik',
						'data' => Tower::model()->getChart('pie-pemilik')
					),
				),
			)
		));?>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th class="text-center">Nama Pemilik</th>
						<th class="text-center">Total Tower</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach(Perusahaan::model()->findAllByAttributes(array('pemilik'=>1)) as $data) { ?>
					<tr>
						<td><?php print $data->nama; ?></td>
						<td class="text-center"><span class="badge"><?php print Tower::model()->countByAttributes(array('id_pemilik'=>$data->id)); ?></span></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<div>&nbsp;</div>

<?php $this->beginWidget('booster.widgets.TbPanel', array(
		'title' => 'Sebaran Tower Berdasarkan Penyewa',
		'headerIcon' => 'list',
)); ?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'title' => array(
					'text' => 'Sebaran Tower Berdasarkan Penyewa',
				),
				'xAxis' => array(
					'categories' => ['Penyewa Tower']
				),
				'yAxis' => array(
					'title' => array(
						'text' => 'Total',
					),
				),
				'series' => Tower::model()->getChart('bar-penyewa')
			),
		));?>
	</div>
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'theme' => '', 
				'title' => array(
					'text' => 'Sebaran Tower Berdasarkan Penyewa'
				),
				'series' => array(
					array(
						'type' => 'pie',
						'name' => 'Sebaran Tower Berdasarkan Penyewa',
						'data' => Tower::model()->getChart('pie-penyewa')
					),
				),
			)
		));?>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th class="text-center">Nama Penyewa</th>
						<th class="text-center">Total Tower</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach(Perusahaan::model()->findAllByAttributes(array('penyewa'=>1)) as $data) { ?>
					<tr>
						<td><?php print $data->nama; ?></td>
						<td class="text-center"><span class="badge"><?php print Tower::model()->countByAttributes(array('id_penyewa'=>$data->id)); ?></span></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>

<div>&nbsp;</div>

<?php $this->beginWidget('booster.widgets.TbPanel', array(
		'title' => 'Sebaran Tower Berdasarkan Kecamatan',
		'headerIcon' => 'list',
)); ?>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'title' => array(
					'text' => 'Sebaran Tower Berdasarkan kecamatan',
				),
				'xAxis' => array(
					'categories' => ['Kecamatan']
				),
				'yAxis' => array(
					'title' => array(
						'text' => 'Total',
					),
				),
				'series' => Tower::model()->getChart('bar-kecamatan')
			),
		));?>
	</div>
	<div class="col-lg-6 col-md-6">
		<?php $this->widget('booster.widgets.TbHighCharts', array(
			'options' => array(
				'theme' => '', 
				'title' => array(
					'text' => 'Sebaran Tower Berdasarkan Kecamatan'
				),
				'series' => array(
					array(
						'type' => 'pie',
						'name' => 'Sebaran Tower Berdasarkan Kecamatan',
						'data' => Tower::model()->getChart('pie-kecamatan')
					),
				),
			)
		));?>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th class="text-center">Nama Kecamatan</th>
						<th class="text-center">Total Tower</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach(Kecamatan::model()->findAll() as $data) { ?>
					<tr>
						<td><?php print $data->kecamatan; ?></td>
						<td class="text-center"><span class="badge"><?php print Tower::model()->countByAttributes(array('id_kecamatan'=>$data->id)); ?></span></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>