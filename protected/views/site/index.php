<?php
	$this->pageTitle= 'Pengaduan Menara Telekomunikasi';

?>
<h5 style="font-weight:bold">Silakan masukkan pengaduan terkait layanan menara telekomunikasi.</h5>

<div>&nbsp;</div>

<div class="well">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm',array(
            'id' => 'verticalForm',
       	    'type' => 'horizontal',
       	    'enableAjaxValidation'=>true
)); ?>
    
    
    <?php echo $form->errorSummary($model); ?>
    
    <?php echo $form->textFieldGroup($model, 'nama'); ?>
    <?php echo $form->TextFieldGroup($model, 'email'); ?>
    <?php echo $form->TextFieldGroup($model, 'telepon'); ?>
    <?php echo $form->dropDownListGroup($model,'id_pengaduan_kategori',array(
            'widgetOptions'=>array(
                'data' => CHtml::ListData(PengaduanKategori::model()->findAll(), 'id', 'nama'),
                'htmlOptions'=>array('empty'=>'-- Pilih Kategori --')
            )
    ));  ?>
    <?php echo $form->textAreaGroup($model, 'keluhan',array(
            'widgetOptions'=>array(
                'htmlOptions'=>array('rows'=>3)
            )
    )); ?>
    
    <div class="form-actions" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'url'=>array('site/index'),
            'icon'=>'ok',
			'label'=>'Kirim Pengaduan',
	)); ?>
    </div>

<?php $this->endWidget(); ?>
</div>