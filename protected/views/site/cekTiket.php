
<h5 style="font-weight:bold">Silakan masukkan kode tiket pengaduan anda</h5>

<div>&nbsp;</div>

<div id="carikode">

	<?php print CHtml::beginForm(array('site/cekTiket'),'GET'); ?>

	<div class="form-group">
		<?php print CHtml::label('Kode',''); ?>
		<?php print CHtml::TextField('kode','',array('class'=>'form-control','style'=>'width: 100%;'));?>
	</div>

	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'icon' => 'search',
			'label'=>'Cari Pengaduan',
	)); ?>

	<?php print CHtml::endForm(); ?>

</div><!-- #carikode -->


<?php if(isset($_GET['kode'])) { ?>
<?php $kode = $_GET['kode']; ?>
<?php $pengaduan = Pengaduan::model()->findByAttributes(array('kode'=>$kode)); ?>

<?php
	if($pengaduan !== null) {
		Yii::app()->user->setFlash('success','Pengaduan ditemukan, berikut ini informasi pengaduan anda');
		
	} else {
		Yii::app()->user->setFlash('danger','Mohon maaf, kode tiket yang anda masukkan tidak ditemukan dalam data pengaduan kami');		
	}
?>

<?php if($pengaduan !== null)  { ?>

<h3>Hasil Pencarian</h3>

<hr>

<h4 style="font-weight:bold">Pengaduan</h4>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$pengaduan,
		'type' => 'striped bordered',
		'attributes'=>array(
			'kode',
			'nama',
			'email',
			'telepon',
			array(
				'label'=>'Kategori',
				'value'=>$pengaduan->getKategori()
			),
			'keluhan',
			array(
				'label'=>'Status',
				'value'=>$pengaduan->getStatus()
			),
			'waktu_dibuat',
		),
));?> 

<div>&nbsp</div>

<h4 style="font-weight:bold">Tanggapan</h4>

<?php $i=1; foreach(Tanggapan::model()->findAllByAttributes(array('id_pengaduan'=>$pengaduan->id)) as $tanggapan) { ?>

<div>&nbsp</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$tanggapan,
		'type' => 'striped bordered',
		'attributes'=>array(
			'pengirim',
			'tanggapan',
			'waktu_dibuat',
		),
)); ?>
<?php $i++; } ?>
<div>&nbsp</div>

<?php $this->widget('booster.widgets.TbButton',array(
		'label' => 'Beri Tanggapan',
		'context' => 'primary',
		'icon'=>'pencil',
		'htmlOptions' => array(
			'data-toggle' => 'modal',
			'data-target' => '#myModal',
		),
)); ?>

<?php $this->beginWidget('booster.widgets.TbModal',array(
		'id'=>'myModal'
)); ?>
	
	<?php print CHtml::beginForm(array('site/tanggapanPengguna'),'GET',array('target'=>'_blank')); ?>
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Kirim Tanggapan</h4>
    </div>
 
    <div class="modal-body">
		<div style="width:300px;margin-left:auto;margin-right:auto;">
			
			<?php print CHtml::label('Form Tanggapan',''); ?><Br>
			<?php print CHtml::textArea('tanggapan','',array('class'=>'form-control'));?>
			<?php print CHtml::hiddenField('id_pengaduan',$pengaduan->id,array('class'=>'form-control'));?>
			<?php print CHtml::hiddenField('pengirim',$kode,array('class'=>'form-control'));?>
			
		</div>
    </div>
 
    <div class="modal-footer">
        <?php $this->widget('booster.widgets.TbButton',array(
				'buttonType'=>'submit',
                'context' => 'success',
                'label' => 'Kirim Tanggapan',
				'icon'=>'ok',
				//'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
	<?php print CHtml::endForm(); ?>
 
<?php $this->endWidget(); 

} ?>

<?php } ?>