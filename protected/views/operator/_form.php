<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'operator-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="well">
	<?php echo $form->dropDownListGroup($model,'id_perusahaan',array(
			'widgetOptions'=>array(
				'htmlOptions'=>array('class'=>'span5'),
				'data'=>CHtml::listData(Perusahaan::model()->findAll(),'id','nama')
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	</div>
	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
