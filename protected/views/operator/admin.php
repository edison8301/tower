<?php
$this->breadcrumbs=array(
	'Operators'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Operator','url'=>array('index')),
array('label'=>'Create Operator','url'=>array('create')),
);

?>

<h1>Kelola Operator</h1>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'operator-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'header'=>'Perusahaan',
				'name'=>'id_perusahaan',
				'value'=>'$data->getRelation("perusahaan","nama")',
				'filter'=>CHtml::listData(Perusahaan::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>

<div>&nbsp;</div>


<div class="well">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'size'=>'small',
		'label'=>'Tambah Operator',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create')
	)); ?>&nbsp;


</div>