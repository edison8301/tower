<?php
$this->breadcrumbs=array(
	'Operators',
);

$this->menu=array(
array('label'=>'Create Operator','url'=>array('create')),
array('label'=>'Manage Operator','url'=>array('admin')),
);
?>

<h1>Operators</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
