<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="row">
<div class="col-md-4">
<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading">Jumlah Pengaduan</div>
  <div class="panel-body">
  <?php //$this->renderPartial('partisipasi'); ?>
  </div>
  </div>
	<?php $this->widget('booster.widgets.TbPanel',array(
        'title' => 'Kalender',
    	'context' => 'primary',
        'headerIcon' => 'home',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse'
    )); ?>

	<?php $this->widget('booster.widgets.TbPanel',array(
        'title' => 'Login Admin',
    	'context' => 'primary',
        'headerIcon' => 'home',
        'content' => 'login'
    )); ?>

</div>
<div id="form">
<div class="col-md-8">
	<div>
	<?php $this->widget('booster.widgets.TbBreadcrumbs',array(
        'homeLink' => 'Home',
        'links' => array('')
    )); ?>
	</div>

	<div>
		<?php print $content; ?>
	</div>
	</div>
</div>
</div>


<?php $this->endContent(); ?>