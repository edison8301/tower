<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
</head>

<body>

<div id="header">
	<div id="logo">
		<?php print CHtml::image(Yii::app()->baseUrl."/images/logo-admin.png"); ?>
	</div>
</div>
<div id="mainnav">
<?php $this->widget('booster.widgets.TbNavbar',array(
			'brand' => '',
			'fixed' => false,
			'fluid' => true,
			'items' => array(
				array(
					'class' => 'booster.widgets.TbMenu',
					'type' => 'navbar',
					'items' => array(
						array('label' => 'Dashboard','icon'=>'home', 'url' => array('admin/index')),
						array('label' => 'Change Password','icon'=>'lock', 'url' => array('user/changePassword')),
						array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
					)
				)
			)
)); ?>
</div>
	
<div class="containers" id="page">

	<div class="row">
		<div class="col-lg-12">
			<?php echo $content; ?>
		</div>
	</div>


	<div id="footer" style="margin-top:30px;text-align:center;padding:10px">
		Copyright &copy; 2015 by Pemerintah Kabupaten Serang
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
