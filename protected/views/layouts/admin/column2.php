<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin/main'); ?>

<div class="row">
	<div class="col-md-2">
	<?php if(User::isAdmin()) { ?>
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'items' => array(
				array('label' => 'Dashboard','icon'=>'home', 'url' => array('admin/index')),
				array('label' => 'Data Tower','icon'=>'search', 'url' => array('tower/admin')),
				array('label' => 'Tambah Tower','icon'=>'plus', 'url' => array('tower/create')),
				array('label' => 'Peta Tower','icon'=>'globe', 'url' => array('tower/peta')),
				array('label' => 'Zona Sebaran','icon'=>'globe', 'url' => array('zonaSebaran/admin')),
				array('label' => 'Retribusi','icon'=>'file', 'url' => array('retribusi/admin')),
				array('label' => 'Laporan','icon'=>'file', 'url' => array('tower/export')),
				//array('label' => 'Tambah Retribusi','icon'=>'file', 'url' => array('retribusi/createOto')),
				array('label' => 'Perusahaan','icon'=>'list', 'url' => array('perusahaan/admin')),
				array('label' => 'Operator','icon'=>'signal', 'url' => array('operator/admin')),
				array('label' => 'Kecamatan','icon'=>'map-marker', 'url' => array('kecamatan/admin')),
				array('label' => 'Desa','icon'=>'map-marker', 'url' => array('desa/admin')),
				array('label' => 'User','icon'=>'user', 'url' => array('user/admin')),
				array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
			)
	)); ?>
	<?php } elseif(User::isPerizinan()) { ?>
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'items' => array(
				array('label' => 'Dashboard','icon'=>'home', 'url' => array('admin/index')),
				array('label' => 'Data Tower','icon'=>'search', 'url' => array('tower/admin')),
				array('label' => 'Peta Tower','icon'=>'globe', 'url' => array('tower/peta')),
				//array('label' => 'Tambah Retribusi','icon'=>'file', 'url' => array('retribusi/createOto')),
				array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
			)
	)); ?>
	<?php } else { ?>
	<?php $this->widget('booster.widgets.TbMenu',array(
			'type'=>'inverse',
			'items' => array(
				array('label' => 'Profil','icon'=>'user', 'url' => array('perusahaan/index')),
				array('label' => 'Peta Tower','icon'=>'globe', 'url' => array('tower/peta')),
				array('label'=>'Logout ('.Yii::app()->user->id.')','icon'=>'off','url'=>array('site/logout'),'visible'=>!Yii::app()->user->isGuest)
			)
	)); ?>
	<?php } ?>
	</div>

	<div class="col-md-10">
		<div class="row">
			<div class="col-lg-12">
				<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
					<div class="alert alert-<?php print $key; ?>"><?php print $message; ?></div>
				<?php } ?>
				<?php Yii::app()->clientScript->registerScript('hideAlert',
						'$(".alert").animate({opacity: 1.0}, 3000).fadeOut("slow");',
						CClientScript::POS_READY
				); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<?php echo $content; ?>
			</div>
		</div>
	</div><!-- content -->
</div>



<?php $this->endContent(); ?>