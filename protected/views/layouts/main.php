<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/fonts.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/flaticon.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/main.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>


<div id="wrapper">
	<div class="container" id="page">
		<div class="row" id="header">
			<div id="logo">
				<img src="<?php print Yii::app()->baseUrl; ?>/images/logo.png">
			</div>
		</div><!-- header -->
			
		<div id="mainnav">
			<?php $this->widget('booster.widgets.TbNavbar',array(
                    'brand' => '',
                    'fixed' => false,	
    	            'fluid' => true,
                    'type'=>'primary',
                    'items' => array(
                        array(
                            'class' => 'booster.widgets.TbMenu',
            	            'type' => 'navbar',
                            'items' => array(
                                array('label' => 'Kirim Pengaduan', 'url' => array('site/index'), 'icon'=>'bullhorn'),
                                array('label' => 'Cek Pengaduan', 'url' => array('site/cekTiket'), 'icon'=>'search'),
                                array('label' => 'Login', 'url' => array('site/login'), 'icon'=>'lock'),
                            
                            )
                        )
                    )
            )); ?>
        </div>
		<div id="content">
            <?php 
                foreach (Yii::app()->user->getFlashes() as $type => $flash) {
                echo "<div class='alert alert-".$type."'>{$flash}</div>";}
            ?>
			<?php echo $content; ?>
		</div><!-- #content -->
    </div><!-- container -->
</body>
</html>
