<?php
$this->breadcrumbs=array(
	'Desas'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Desa</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Desa',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('desa/create')
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'desa-grid',
		'type' => 'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			'nama',
array(
'class'=>'booster.widgets.TbButtonColumn',
'htmlOptions' => array('style' => 'width: 80px;')
),
),
)); ?>
