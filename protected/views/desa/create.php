<?php
$this->breadcrumbs=array(
	'Desas'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Desa','url'=>array('index')),
array('label'=>'Manage Desa','url'=>array('admin')),
);
?>

<h1>Tambah Desa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>