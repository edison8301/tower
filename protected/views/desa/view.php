<?php
$this->breadcrumbs=array(
	'Desas'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Desa','url'=>array('index')),
array('label'=>'Create Desa','url'=>array('create')),
array('label'=>'Update Desa','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Desa','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Desa','url'=>array('admin')),
);
?>

<h1>Lihat <?php echo $model->nama; ?></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('desa/update','id'=>$model->id)
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('desa/create')
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola Desa',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('desa/admin')
)); ?>&nbsp;

<div> &nbsp; </div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'nama',
),
)); ?>
