<?php
$this->breadcrumbs=array(
	'Desas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Desa','url'=>array('index')),
	array('label'=>'Create Desa','url'=>array('create')),
	array('label'=>'View Desa','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Desa','url'=>array('admin')),
	);
	?>

	<h1>Sunting <?php echo $model->nama; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>