<h1><i>Peta Sebaran Menara Di Kabupaten Serang</i></h1>

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'type'=>'inline',
	'htmlOptions'=>array('class'=>'well form-inline'),
)); ?>
	
	<?php echo $form->dropDownListGroup($search,'pemilik',array(
			'wrapperHtmlOptions'=>array('class'=>'col-sm-1'),
			'widgetOptions'=>array('data'=>CHtml::listData(Perusahaan::model()->findAllByAttributes(array('pemilik'=>1)),'id','nama'),
			'htmlOptions'=>array('empty'=>'-- Semua Pemilik --'))
	)); ?>


	<?php echo $form->select2Group($search,'penyewa',array(
					'wrapperHtmlOptions'=>array(),
					'widgetOptions' => array(
						'asDropDownList' => false,
						'options' => array(
							'tags'=>Tower::model()->getData(),
							'placeholder' => 'Semua Penyewa',
							'separator' => ';'
						)
					)
	)); ?>

	<?php echo $form->select2Group($search,'operator',array(
					'wrapperHtmlOptions'=>array(),
					'widgetOptions' => array(
						'asDropDownList' => false,
						'options' => array(
							'tags'=>Tower::model()->getListOperator(),
							'placeholder' => 'Semua Penyewa',
							'separator' => ';'
						)
					)
	)); ?>

	<?php /*echo $form->dropD($search,'penyewa',array(
				'widgetOptions'=>array('data'=>CHtml::listData(Perusahaan::model()->findAllByAttributes(array('penyewa'=>1)),'id','nama'),
				'htmlOptions'=>array('class'=>'span5','empty'=>'-- Semua Penyewa --'))
	)); */ ?>

	<?php echo $form->dropDownListGroup($search,'kecamatan',array(
			'widgetOptions'=>array('data'=>CHtml::listData(Kecamatan::model()->findAll(),'id','kecamatan'),
				'htmlOptions'=>array('class'=>'span5','empty'=>'-- Semua Kecamatan --'))
	)); ?>

	<?php echo $form->dropDownListGroup($search,'desa',array(
			'widgetOptions'=>array('data'=>CHtml::listData(Desa::model()->findAll(),'id','nama'),
			'htmlOptions'=>array('class'=>'span5','empty'=>'-- Semua Desa --'))
	)); ?>
	
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'submit',
		'context'=>'primary',
		'icon'=>'search',
		'label'=>'Filter',
	)); ?>
		
<?php $this->endWidget(); ?>

<div>&nbsp;</div>

<?php 
	Yii::import('ext.gmap.*');
	$gMap = new EGMap();
	$gMap->width = '100%';
	$gMap->height = 450;
	$gMap->zoom = 12;
	$mapTypeControlOptions = array(
		'position'=> EGMapControlPosition::LEFT_BOTTOM,
		'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU
	);
 
	$gMap->mapTypeControlOptions= $mapTypeControlOptions;
 	
 	if(!empty($_GET['latlong']))
	{	
		$lokasi = explode(',',$_GET['latlong']);
		$lat = trim($lokasi[0]);
		$long = trim($lokasi[1]);

		$gMap->setCenter($lat,$long);
		$iconPusat = new EGMapMarkerImage("http://previewaplikasi.com/mapIcon/pin.png");
	

		$center = new EGMapMarker($lat,$long,array('title' => 'Lokasi Pencarian','icon'=>$iconPusat));
		$gMap->addMarker($center);
		
	} else {
		$gMap->setCenter(-6.12000,106.15028);
	}
 
	$icon = new EGMapMarkerImage("http://previewaplikasi.com/mapIcon/radio-station-2.png");
	$iconhabis = new EGMapMarkerImage("http://previewaplikasi.com/mapIcon/radio-station-3.png");
	$iconwarning = new EGMapMarkerImage("http://previewaplikasi.com/mapIcon/radio-station-4.png");
	$bulankurang = date('m')-1;
	$tanggal_warning = date('Y-'.$bulankurang.'-d');
	
	$criteria=new CDbCriteria;
	if(isset($_GET['Search'])) {

		$criteria->compare('id_operator',$_GET['Search']['operator'],true);
		$criteria->compare('id_pemilik',$_GET['Search']['pemilik']);
		$criteria->compare('id_penyewa',$_GET['Search']['penyewa']);
		$criteria->compare('id_kecamatan',$_GET['Search']['kecamatan']);
		$criteria->compare('id_kecamatan',$_GET['Search']['desa']);

		if(User::isPerusahaan())
		{
			$criteria->compare('id_pemilik',User::getIdPemilikByUserId());
		}
	}
	$model = Tower::model()->findAll($criteria); 
	
	foreach($model as $data) {
		$koordinat = explode(',',$data->koordinat);
		$tahun = date('Y-m-d');
		$lat = $koordinat[0];
		$long = $koordinat[1];
		
		if($data->habis_izin != null ) {

		if($data->habis_izin > $tahun )
		{
			$marker = new EGMapMarker($lat,$long,array('title' => 'Lokasi Tower '.$data->getRelationField('Operator','nama'),'icon'=>$icon));
		}
		elseif($data->habis_izin > $tanggal_warning AND $data->habis_izin < $tahun)
		{
			$marker = new EGMapMarker($lat,$long,array('title' => 'Lokasi Tower '.$data->getRelationField('Operator','nama'),'icon'=>$iconwarning));
		}
		else
		{
			$marker = new EGMapMarker($lat,$long,array('title' => 'Lokasi Tower '.$data->getRelationField('Operator','nama'),'icon'=>$iconhabis));
		}

		} else {
			$marker = new EGMapMarker($lat,$long,array('title' => 'Lokasi Tower '.$data->getRelationField('Operator','nama'),'icon'=>$icon));
		}
		
		$detail = new EGMapInfoWindow($data->getDetailMarker($data->id));
		
		$marker->addHtmlInfoWindow($detail);
		$gMap->addMarker($marker);
	}

	foreach(ZonaSebaran::model()->findAll() as $zona)
	{
		$coord = new EGMapCoord($zona->lattitude,$zona->longitude);
		$detail = new EGMapInfoWindow($zona->no_site);
		$circle = new EGMapCircle($coord);
		$circle->radius = 300;
		$circle->addHtmlInfoWindow($detail);

		$gMap->addCircle($circle);
	}
 
	$gMap->renderMap();
?>

<hr>

<div class="bs-callout bs-callout-primary">
  <h4>Legenda</h4>
  <table>
	<tr>
		<td><?php print CHtml::image(Yii::app()->baseUrl.'/mapIcon/radio-station-2.png','',array('class'=>'img-responsive')); ?></td>
		<td><span class="label label-primary">Tower Dengan Masa Izin</span></td>
	</tr>
	<tr>
		<td><?php print CHtml::image(Yii::app()->baseUrl.'/mapIcon/radio-station-3.png','',array('class'=>'img-responsive')); ?></td>
		<td><span class="label label-danger">Tower Yang Sudah Habis Masa Izin</span></td>
	</tr>
	<tr>
		<td><?php print CHtml::image(Yii::app()->baseUrl.'/mapIcon/radio-station-4.png','',array('class'=>'img-responsive')); ?></td>
		<td><span class="label label-warning">Tower Yang Akan Habis Masa Izin</span></td>
	</tr>
	<tr>
		<td><?php print CHtml::image(Yii::app()->baseUrl.'/mapIcon/sebaran.png','',array('class'=>'img-responsive')); ?></td>
		<td><span class="label label-danger">Zona Sebaran Berdasarkan Peraturan Bupati</span></td>
	</tr>
  </table>
</div>

<hr>

<h3>Cari Lokasi<h3>

<div class="well">
	<?php print CHtml::beginForm(array('tower/peta'),'GET'); ?>
	<input type="text" name="latlong" class="form-control" placeholder="Latitude, Longitude" style="display:inline;width:250px">
	
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'submit',
		'context'=>'primary',
		'icon'=>'search',
		'label'=>'Filter',
	)); ?>

	<?php print CHtml::endForm(); ?>	
</div>