<?php
$this->breadcrumbs=array(
	'Towers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Tower','url'=>array('index')),
	array('label'=>'Create Tower','url'=>array('create')),
	array('label'=>'View Tower','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Tower','url'=>array('admin')),
	);
	?>

	<h1>Sunting Tower</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>