
<h1>Export Data Tower</h1>
<div> &nbsp; </div>
<?php print CHtml::beginForm(array('tower/exportExcel')); ?>

<div class="well">
<div class="form-group">
<?php Print CHtml:: dropDownList('id_operator','',CHtml::listData(Perusahaan::model()->findAllByAttributes(array('operator'=>1)),'id','nama'), array('class' => 'form-control', 'empty' => '-- Semua Operator --')); ?>
</div>

<div class="form-group">
<?php Print CHtml:: dropDownList('id_pemilik','',CHtml::listData(Perusahaan::model()->findAllByAttributes(array('pemilik'=>1)),'id','nama'), array('class' => 'form-control', 'empty' => '-- Semua Pemilik --')); ?>
</div>

<div class="form-group">
<?php Print CHtml:: dropDownList('id_penyewa','',CHtml::listData(Perusahaan::model()->findAllByAttributes(array('penyewa'=>1)),'id','nama'),array('class'=>'form-control', 'empty' => '-- Semua Penyewa --')); ?>
</div>

<div class="form-group">
<?php Print CHtml:: dropDownList('id_kecamatan','',CHtml::listData(Kecamatan::model()->findAll(),'id','kecamatan'),array('class'=>'form-control', 'placeholder' => 'Lokasi', 'empty' => '-- Semua Lokasi --')); ?>
</div>
</div>

	<div class="form-actions well">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Proses',
		)); ?>
	</div>
<?php print CHtml::endForm(); ?>


