<table  cellpadding="0" cellspacing="4" border="0">
	<form name="lat_long_form">
	<tr>
		<td colspan="3"><span class="style2"><b>Latitude:</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" class="" name="decLt" value="" size="20"> degrees
		</td>
		<td><b style="font-size:20px">=</b></td>
		<td nowrap="nowrap">
			<input type="text" class="formfield" name="dLt" value="" size="2" maxlength="3" style="width:40px;margin-right:2px">deg &nbsp;<input type="text" class="formfield" name="mLt" value="" size="2" maxlength="3" style="width:40px;margin-right:2px">min &nbsp;<input type="text" class="formfield" name="sLt" value="" size="2" style="width:40px;margin-right:2px">sec &nbsp;<select name="dirLt" class="style2">
				<option value="S">S
				<option value="N">N
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3" style="padding-top:8px;border-top: 1px solid #000000"><b>Longitude:</b></td>
	</tr>
	<tr>
		<td>
			<input type="text" class="" name="decLg" value="" size="20"> degrees
		</td>
		<td><b style="font-size:20px">=</b></td>
		<td nowrap="nowrap">
			<input type="text" class="formfield" name="dLg" value="" size="2" maxlength="3" style="width:40px;margin-right:2px">deg &nbsp;<input type="text" class="formfield" name="mLg" value="" size="2" maxlength="3" style="width:40px;margin-right:2px">min &nbsp;<input type="text" class="formfield" name="sLg" value="" size="2" style="width:40px;margin-right:2px">sec &nbsp;<select name="dirLg" class="style2">
				<option value="E">E
				<option value="W">W
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center" nowrap="nowrap">
			<table cellpadding="0" cellspacing="5" border="0">
				<tr>
					<td valign="top"><input type="button" class="inputBtn1 btn btn-primary btn-xs" name="" value="Konversi DD Ke Format DMS =>" onclick="convertAll('>>');return false;"></td>
				</tr>
				<tr>
					<td><input type="button" class="inputBtn1 btn btn-primary btn-xs" name="" value="<= Konversi DMS Ke Format DD" onclick="convertAll('<<');return false;"></td>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>

<script language="JavaScript" type="text/javascript">
// degrees, mins, secs to decimal degrees
function dms_to_d(d,m,s,dir)
{
	//alert( "d:"+d+" m:"+m+" s:"+s );
	d = d-0;
	m = m-0;
	var sign = ( dir=='W' || dir=='S' ) ? -1 : 1;
	return (((s/60+m)/60)+d)*sign;
}
function d_to_dms(d,x)
{
	var a = new Array();
	if(x=='Lt') {
		a['dir'] = (d>=0)?'N':'S';
	} else if(x=='Lg') {
		a['dir'] = (d>=0)?'E':'W';
	}
	if(d<0) { d=Math.abs(d); }
	var n = d+'';
	var tmp = n.split(".");  if(tmp[0]=='') { tmp[0]='0' }
	a['d'] = tmp[0]; // n
	n = parseFloat("."+tmp[1])*60+''; // min dec
	tmp = n.split(".");
	a['m'] = tmp[0]; // min
	n = parseFloat("."+tmp[1])*60+''; // sec dec
	tmp = n.split(".");
	a['s'] = tmp[0]; // sec
	return a;
}
function convertAll(direction)
{
	convertLatLong(direction,"Lt");
	convertLatLong(direction,"Lg");
}
function convertLatLong(direction,L)
{
	var f = document.lat_long_form;

	if(direction=='<<') {
		var d = f.elements['d'+L].value;
		var m = f.elements['m'+L].value;
		var s = f.elements['s'+L].value;
		var tmp = f.elements['dir'+L].options;
		var dir = tmp[tmp.selectedIndex].value;
		var ans = rnd(dms_to_d(d,m,s,dir),4);
		f.elements['dec'+L].value = ans;
		document.getElementById("Tower_koordinat").value = f.elements['decLt'].value + "," + f.elements['decLg'].value;
	} else {
		var dec = f.elements['dec'+L].value;
		var a = d_to_dms(dec,L);
		f.elements['d'+L].value = isNaN(a['d'])?'0':a['d'];
		f.elements['m'+L].value = isNaN(a['m'])?'0':a['m'];
		f.elements['s'+L].value = isNaN(a['s'])?'0':a['s'];
		if(a['dir']) {
			setSelectedValue(f.elements['dir'+L],a['dir']);
		}
	}
	return;

}
function rnd(v, p)
{
	if(rnd.arguments.length==1) { p=0; }
	v = v * Math.pow(10,p);
	v = Math.round(v);
	v = v / Math.pow(10,p);
	return v;
}
function setSelectedValue (select,val)
{	for (var i = 0; i < select.options.length; i++)
	{	if (select.options[i].value==val) {
		 	select.options.selectedIndex=i;
		 	break;
		}
	}
}
</script>