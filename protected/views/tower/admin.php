<h1>Kelola Tower</h1>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'tower-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'name'=>'id_pemilik',
				'header'=>'Pemilik',
				'value'=>'$data->getRelationField("Pemilik","nama")',
				'filter'=>CHtml::listData(Perusahaan::model()->findAllByAttributes(array('pemilik'=>1)),'id','nama')
			),
			array(
				'name'=>'id_penyewa',
				'header'=>'Penyewa',
				'value'=>'$data->id_penyewa',
			),
			array(
				'name'=>'id_operator',
				'header'=>'Operator',
				'value'=>'$data->id_operator',
			),
			array(
				'name'=>'id_kecamatan',
				'header'=>'Kecamatan',
				'value'=>'$data->getRelationField("Kecamatan","kecamatan")',
				'filter'=>CHtml::listData(Kecamatan::model()->findAll(),'id','kecamatan')
			),
			array(
				'name'=>'id_desa',
				'header'=>'Desa',
				'type'=>'raw',
				'filter'=>CHtml::listData(Desa::model()->findAll(),'id','nama'),
				'value'=>'$data->getDesa()',
			),
			//'koordinat',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'visible'=>User::isAdmin(),
				'headerHtmlOptions'=>array('style'=>'width:8%')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}',
				'visible'=>!User::isAdmin(),
				'headerHtmlOptions'=>array('style'=>'width:8%')
			),
		),
)); ?>

<div>&nbsp;</div>

<?php if(User::isAdmin()) { ?>
<div class="well" style="text-align:right">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Tower',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('create')
)); ?>&nbsp;
</div>
<?php } ?>
