<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_operator')); ?>:</b>
	<?php echo CHtml::encode($data->id_operator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pemilik')); ?>:</b>
	<?php echo CHtml::encode($data->id_pemilik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_penyewa')); ?>:</b>
	<?php echo CHtml::encode($data->id_penyewa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat')); ?>:</b>
	<?php echo CHtml::encode($data->alamat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kecamatan')); ?>:</b>
	<?php echo CHtml::encode($data->id_kecamatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('luas_lahan')); ?>:</b>
	<?php echo CHtml::encode($data->luas_lahan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('koordinat')); ?>:</b>
	<?php echo CHtml::encode($data->koordinat); ?>
	<br />

	*/ ?>

</div>