<div>&nbsp;</div>


<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tower-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="well">
	<div class="row">
		<div class="col-sm-6">
			
			<?php echo $form->dropDownListGroup($model,'id_pemilik',array('widgetOptions'=>array('data'=>CHtml::listData(Perusahaan::model()->findAllByAttributes(array('pemilik'=>1)),'id','nama'),'htmlOptions'=>array('class'=>'span5','empty'=>'-- Pilih Pemilik --')))); ?>
	
			<?php //echo $form->dropDownListGroup($model,'id_penyewa',array('widgetOptions'=>array('data'=>CHtml::listData(Perusahaan::model()->findAllByAttributes(array('penyewa'=>1)),'id','nama'),'htmlOptions'=>array('class'=>'span5','empty'=>'-- Pilih Penyewa --')))); ?>
			
			<?php echo $form->select2Group($model,'id_penyewa',array(
					'wrapperHtmlOptions'=>array(),
					'widgetOptions' => array(
						'asDropDownList' => false,
						'options' => array(
							'tags'=>Tower::model()->getData(),
							'placeholder' => 'Penyewa',
							'separator' => ';'
						)
					)
			));?>

			<?php echo $form->select2Group($model,'id_operator',array(
					'wrapperHtmlOptions'=>array(),
					'widgetOptions' => array(
						'asDropDownList' => false,
						'options' => array(
							'tags'=>Tower::model()->getListOperator(),
							'placeholder' => 'Penyewa',
							'separator' => ';'
						)
					)
			));?>

			
			<?php echo $form->textAreaGroup($model,'alamat', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

			<?php echo $form->dropDownListGroup($model,'id_kecamatan',array('widgetOptions'=>array('data'=>CHtml::listData(Kecamatan::model()->findAll(),'id','kecamatan'),'htmlOptions'=>array('class'=>'span5','empty'=>'-- Pilih Kecamatan --')))); ?>

		</div>
		<div class="col-sm-6">
			
			<?php echo $form->dropDownListGroup($model,'id_desa',array('widgetOptions'=>array('data'=>CHtml::listData(Desa::model()->findAll(),'id','nama'),'htmlOptions'=>array('class'=>'span5','empty'=>'-- Pilih Desa --')))); ?>
		
			<?php echo $form->textFieldGroup($model,'luas_lahan',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)),'append' => 'M<sup>2</sup>')); ?>
	
			<?php echo $form->textFieldGroup($model,'koordinat',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
			<?php echo $form->textFieldGroup($model,'njop',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
			
			<?php echo $form->textFieldGroup($model,'nomor_izin',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>20)))); ?>
			
			<?php echo $form->datePickerGroup($model,'habis_izin',array(
					'wrapperHtmlOptions'=>array('class'=>'col-sm-12'),
					'widgetOptions'=>array('options'=>array('format' => 'yyyy-mm-dd','autoclose'=>true),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>'
			)); ?>
			
			<?php echo $form->textAreaGroup($model,'kondisi_sekitar',array(
					'widgetOptions'=>array(
						'htmlOptions'=>array('rows'=>'3')
					)
			)); ?>
		</div>
	</div>
</div>
	<div class="well">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
	</div>	
	
<?php $this->endWidget(); ?>




<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbPanel',array(
    'title' => 'DMS To DD Converter',
    'context' => 'primary',
    'headerIcon' => 'sort',
    'content' => $this->renderPartial('_converter',array(),true)
)); ?>