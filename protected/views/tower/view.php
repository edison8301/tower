<?php
$this->breadcrumbs=array(
	'Towers'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Tower','url'=>array('index')),
array('label'=>'Create Tower','url'=>array('create')),
array('label'=>'Update Tower','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Tower','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Tower','url'=>array('admin')),
);
?>

<h1>Lihat Tower</h1>



<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered condensed',
'attributes'=>array(
		array(
			'label'=>'Pemilik',
			'value'=>$model->getRelationField('Pemilik','nama')
		),
		array(
			'label'=>'Penyewa',
			'value'=>$model->id_penyewa
		),
		array(
			'label'=>'Operator',
			'value'=>$model->id_operator
		),
		'alamat',
		array(
			'label'=>'Kecamatan',
			'value'=>$model->getRelationField('Kecamatan','kecamatan')
		),
		array(
			'label'=>'Desa',
			'value'=>$model->getDesa()
		),
		array(
			'label'=>'Luas Lahan',
			'type'=>'raw',
			'value'=>Bantu::m2($model->luas_lahan)
		),
		array(
			'label'=>'Kondisi Sekitar',
			'type'=>'raw',
			'value'=>$model->kondisi_sekitar
		),
		array(
			'label'=>'Habis Izin',
			'type'=>'raw',
			'value'=>$model->habis_izin
		),
		array(
			'label'=>'NJOP',
			'type'=>'raw',
			'value'=>Bantu::rp($model->njop)
		),
		'koordinat',
),
)); ?>

<div>&nbsp;</div>

<div class="well">
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting Tower',
		'icon'=>'pencil',
		'context'=>'primary',
		'size'=>'small',
		'visible'=>User::isAdmin(),
		'url'=>array('update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola Tower',
		'icon'=>'list',
		'size'=>'small',
		'context'=>'primary',
		'url'=>array('admin')
)); ?>&nbsp;
</div>

<div>&nbsp;</div>
	<?php $gambar = Gambar::model()->findAllByAttributes(array('id_tower'=>$model->id)); ?>
	<style>
	#map-canvas {
		width: 100%;
		height: 350px;
		text-align:center;
	}
	</style>
	
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
	function initialize() {
	var mapCanvas = document.getElementById('map-canvas');
	var myLatlng = new google.maps.LatLng(<?php print $model->koordinat; ?>);
	var icon = 'http://previewaplikasi.com/mapIcon/radio-station-2.png';
	var contentString = <?php echo json_encode($model->getDetailMarker($model->id)); ?>;
	var infowindow = new google.maps.InfoWindow({
		content: contentString
	});
	var mapOptions = {
			center: myLatlng,
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		var map = new google.maps.Map(mapCanvas, mapOptions);
		
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			icon: icon,
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});
		marker.setMap(map);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	
	</script>
	<div id="map-canvas" class="center-block"></div>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Gambar',
		'icon'=>'plus',
		'context'=>'primary',
		'visible'=>User::isAdmin(),
		'url'=>array('gambar/create','id_tower'=>$model->id)
)); ?>&nbsp;

<div>&nbsp;</div>
<div class="row">
	<?php foreach($gambar as $data) { ?>
		<div class="col-xs-3">
			<?php $gambar = CHtml::image(Yii::app()->baseUrl.'/uploads/gambar/'.$data->file,'',array('class'=>'thumbnail img-responsive')); ?>
			<?php print CHtml::Link($gambar,array('gambar/delete','id'=>$data->id),array('submit'=>array('gambar/delete','id'=>$data->id),'confirm'=>'Anda yakin akan menghapus gambar?','title'=>'Klik untuk menghapus gambar','data-toggle'=>'tooltip','data-placement'=>'bottom')); ?>
		</div>
	<?php } ?>
</div>

<div>&nbsp</div>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Buat Retribusi',
		'icon'=>'plus',
		'context'=>'primary',
		'visible'=>User::isAdmin(),
		'url'=>array('retribusi/createOto','id_tower'=>$model->id)
)); ?>&nbsp;

<div style="overflow:auto">
<table class="table">
<thead>
<tr width="20px">
	<th>No</th>
	<th>Perusahaan</th>
	<th>Tower</th>
	<th>Nomor</th>
	<th>Nama Perusahaan</th>
	<th>Alamat</th>
	<th>Peruntukan</th>
	<th>Lokasi</th>
	<th>Luas Lahan</th>
	<th>NJOP</th>
	<th>Persentase</th>
	<th>Jumlah Operator</th>
	<th>Jumlah Pengawasan</th>
	<th>Jatuh Tempo</th>
	<th>Tanggal Dibuat</th>
</tr>
</thead>

<?php  $i=1; foreach($model->findAllRetribusi() as $retribusi) { ?>
 <tr style="font-size: 11px;">
 	<td><?=$i ?></td>
 	<td><?= $retribusi->getRelationField("Perusahaan","nama"); ?></td>
 	<td><?= $retribusi->getRelationField("Perusahaan","nama"); ?></td>
 	<td><?= $retribusi->nomor; ?></td>
 	<td><?= $retribusi->nama_perusahaan; ?></td>
 	<td><?= $retribusi->alamat_perusahaan; ?></td>
 	<td><?= $retribusi->peruntukan; ?></td>
 	<td><?= $retribusi->lokasi; ?></td>
 	<td><?= $retribusi->luas_lahan; ?></td>
 	<td><?= $retribusi->njop; ?></td>
 	<td><?= $retribusi->persentase; ?></td>
 	<td><?= $retribusi->jumlah_operator; ?></td>
 	<td><?= $retribusi->jumlah_pengawasan; ?></td>
 	<td><?= $retribusi->jatuh_tempo; ?></td>
 	<td><?= $retribusi->tanggal_dibuat; ?></td>

 </tr>


<?php $i++; } ?>
 </table>
</div>