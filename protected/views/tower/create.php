<?php
$this->breadcrumbs=array(
	'Towers'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Tower','url'=>array('index')),
array('label'=>'Manage Tower','url'=>array('admin')),
);
?>

<h1>Tambah Tower</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>