<?php
$this->breadcrumbs=array(
	'Tanggapans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Create',
);

$this->menu=array(
array('label'=>'List Tanggapan','url'=>array('index')),
array('label'=>'Manage Tanggapan','url'=>array('admin')),
);
?>

<h1>Input Tanggapan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>