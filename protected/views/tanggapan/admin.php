<?php
$this->breadcrumbs=array(
	'Tanggapans'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Tanggapan','url'=>array('index')),
array('label'=>'Create Tanggapan','url'=>array('create')),
);
?>

<h1>Kelola Tanggapan</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('tanggapan/create')
)); ?>&nbsp;

</div><!-- search-form -->

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'tanggapan-grid',
'type' => 'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
				 array(
			 	'header' => 'Pengaduan',
			 	'name' => 'id_pengaduan',
			 	'value' => '$data->getPengaduan()',
			 	'headerHtmlOptions' => array('width' =>'10%'),
			 	'htmlOptions'=>array('style'=>'text-align:center'),
			 	'filter' => CHtml::listData(Pengaduan::model()->findAll(array('order'=>'nama ASC')),'id','nama')
			 	),
		'tanggapan',
		'pengirim',
		'waktu_dibuat',
array(
'class'=>'booster.widgets.TbButtonColumn',
'htmlOptions' => array('style' => 'width: 80px;'),
),
),
)); ?>
