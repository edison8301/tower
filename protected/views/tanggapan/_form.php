
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tanggapan-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>


<div class="well">

<?php echo $form->errorSummary($model); ?>

	<?php //echo $form->dropDownListGroup($model,'id_pengaduan',array('widgetOptions'=>array('data' => CHtml::ListData(Pengaduan::model()->findAll(), 'id', 'nama'),'htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textAreaGroup($model,'tanggapan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

	
</div>


	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
