<?php
$this->breadcrumbs=array(
	'Tanggapans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Tanggapan','url'=>array('index')),
	array('label'=>'Create Tanggapan','url'=>array('create')),
	array('label'=>'View Tanggapan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Tanggapan','url'=>array('admin')),
	);
	?>

	<h1>Sunting Tanggapan ID : <b><?php echo $model->id; ?></b></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>