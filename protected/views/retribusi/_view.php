<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_perusahaan')); ?>:</b>
	<?php echo CHtml::encode($data->id_perusahaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tower')); ?>:</b>
	<?php echo CHtml::encode($data->id_tower); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_perusahaan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_perusahaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_perusahaan')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_perusahaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('peruntukan')); ?>:</b>
	<?php echo CHtml::encode($data->peruntukan); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('lokasi')); ?>:</b>
	<?php echo CHtml::encode($data->lokasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('luas_lahan')); ?>:</b>
	<?php echo CHtml::encode($data->luas_lahan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('njop')); ?>:</b>
	<?php echo CHtml::encode($data->njop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('persentase')); ?>:</b>
	<?php echo CHtml::encode($data->persentase); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_operator')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_operator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jumlah_pengawasan')); ?>:</b>
	<?php echo CHtml::encode($data->jumlah_pengawasan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jatuh_tempo')); ?>:</b>
	<?php echo CHtml::encode($data->jatuh_tempo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_dibuat')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_dibuat); ?>
	<br />

	*/ ?>

</div>