<h1>Kelola Retribusi</h1>



<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'retribusi-grid',
		'type'=>'striped bordered condensed',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nomor',
			'nama_perusahaan',
			'lokasi',
			array(
				'name'=>'jatuh_tempo',
				'value'=>'Bantu::tanggal($data->jatuh_tempo)',
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'name'=>'tanggal_dibuat',
				'value'=>'Bantu::tanggal($data->tanggal_dibuat)',
				'htmlOptions'=>array('style'=>'text-align:center'),
				'headerHtmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'visible'=>User::isAdmin()
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}',
				'visible'=>!User::isAdmin()
			),
		),
)); ?>


<div>&nbsp;</div>

<div class="well" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah Retribusi',
		'icon'=>'plus',
		'size'=>'small',
		'context'=>'primary',
		'url'=>array('createOto')
	)); ?>&nbsp;

</div>