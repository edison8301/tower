<?php
$this->breadcrumbs=array(
	'Retribusis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Retribusi','url'=>array('index')),
	array('label'=>'Create Retribusi','url'=>array('create')),
	array('label'=>'View Retribusi','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Retribusi','url'=>array('admin')),
	);
	?>

	<h1>Sunting Retribusi</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>