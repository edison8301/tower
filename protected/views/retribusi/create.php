<?php
$this->breadcrumbs=array(
	'Retribusis'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Retribusi','url'=>array('index')),
array('label'=>'Manage Retribusi','url'=>array('admin')),
);
?>

<h1>Tambah Retribusi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>