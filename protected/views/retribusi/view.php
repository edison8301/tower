<h1>Detail Retribusi</h1>


<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			array(
				'label'=>'Perusahaan',
				'value'=>$model->getRelationField('Perusahaan','nama')
			),
			array(
				'label'=>'Tower',
				'type'=>'raw',
				'value'=>$model->getRelationField('Perusahaan','nama').' | '.$model->getRelationField('Tower','alamat')
			),
			'nomor',
			array(
				'label'=>'Tanggal',
				'type'=>'raw',
				'value'=>Bantu::tanggal($model->tanggal)
			),	
			'nama_perusahaan',
			'alamat_perusahaan',
			'persentase',
			array(
				'label'=>'Jumlah',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->jumlah,'0',',','.')
			),
			array(
				'label'=>'Denda',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->getDenda(),'0',',','.')
			),
			array(
				'label'=>'Total Tagihan',
				'type'=>'raw',
				'value'=>'Rp '.number_format($model->getTotal(),'0',',','.')
			),
			array(
				'label'=>'Jatuh Tempo',
				'type'=>'raw',
				'value'=>Bantu::tanggal($model->jatuh_tempo)
			),
			array(
				'label'=>'Tanggal Dibuat',
				'type'=>'raw',
				'value'=>Bantu::tanggal($model->tanggal_dibuat)
			),
),
)); ?>

<div>&nbsp;</div>



<div class="well">

<?php if(User::isAdmin()) { ?>
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting Retribusi',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('update','id'=>$model->id)
)); ?>&nbsp;
<?php } ?>
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola Retribusi',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('admin')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Cetak Retribusi',
		'icon'=>'print',
		'context'=>'primary',
		'url'=>array('cetak','id'=>$model->id)
)); ?>&nbsp;
</div>

<div>&nbsp;</div>

<hr>

<h2>Data Tower</h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered condensed',
		'attributes'=>array(
			array(
				'label'=>'Tower',
				'type'=>'raw',
				'value'=>$model->getRelationField('Perusahaan','nama').' | '.$model->getRelationField('Tower','alamat')
			),
			'lokasi',
			array(
				'label'=>'Luas Lahan',
				'type'=>'raw',
				'value'=>Bantu::m2($model->luas_lahan)
			),
			array(
				'label'=>'NJOP',
				'type'=>'raw',
				'value'=>Bantu::rp($model->njop)
			),
			'persentase',
			'jumlah_operator',
			'jumlah_pengawasan',
))); ?>