<div class="well">
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'retribusi-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'id_tower',array(
			'widgetOptions'=>array(
				'data'=>Retribusi::listData(),
				'htmlOptions'=>array('class'=>'span5','empty'=>'-- Pilih Tower --')
			),
			'value'=>isset($_GET['id_tower']) ? $_GET['id_tower'] : null
	)); ?>
	
	<?php echo $form->textFieldGroup($model,'nomor',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->datePickerGroup($model,'tanggal',array(
			'widgetOptions'=>array(
				'options'=>array('autoclose'=>true,'format'=>'yyyy-mm-dd'),
				'htmlOptions'=>array('class'=>'span5','value'=>date('Y-m-d'))
			),
			'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>'
	)); ?>

	<?php echo $form->textFieldGroup($model,'jumlah',array(
			'widgetOptions'=>array(
				'htmlOptions'=>array('class'=>'span5','maxlength'=>255),
			),
			'prepend'=>'Rp'
	)); ?>

	<div class="form-actions" style="text-align:right">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Buat Retribusi',
			'icon'=>'ok',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
</div>