<?php

class AdminController extends Controller
{

	public $layout = '//layouts/admin/column2';

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionIndex2()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'waktu_dibuat DESC';
		
		if(!User::isAdmin())
		{
			//$criteria->condition = '(SELECT * FROM pengaduan)';
		}

		$dataProvider=new CActiveDataProvider('Pengaduan',array(
			'criteria'=>$criteria
		));
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	public function actionGrafikBulan()
	{
		
		$this->render('grafikperbulan');

	}
	public function actionGrafikKategori()
	{
		
		$this->render('grafikperkategori');

	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}