<?php

class TowerController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
	public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view','rekap','peta'),
'users'=>array('@'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update','export','exportExcel'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('@'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Tower;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tower']))
		{
			$model->attributes=$_POST['Tower'];
			if($model->save())
			{
				$this->saveTowerPenyewa($model);
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function saveTowerPenyewa($model)
	{
		$values = explode(",",$model->id_penyewa);
		foreach($values as $value)
		{
			$perusahaan = Perusahaan::model()->findByAttributes(array('nama'=>trim($value)));
			$ada = TowerPenyewa::model()->findByAttributes(array('id_perusahaan'=>$perusahaan->id,'id_tower'=>$model->id));
			if($ada === null)
			{
				$TowerPenyewa = new TowerPenyewa;
				$TowerPenyewa->id_tower = $model->id;
				$TowerPenyewa->id_perusahaan = $perusahaan->id;
				$TowerPenyewa->save();
			}
			else
			{
				$ada->id_perusahaan = $perusahaan->id;
				$ada->save();
			}
		}
	}
	
	public function actionRekap()
	{
		$this->render('rekap');
	}
	
	public function actionPeta()
	{

		$search= new Search; 
		
		if(isset($_GET['Search']['operator']))
			$search->operator = $_GET['Search']['operator'];

		if(isset($_GET['Search']['pemilik']))
			$search->pemilik = $_GET['Search']['pemilik'];

		if(isset($_GET['Search']['penyewa']))
			$search->penyewa = $_GET['Search']['penyewa'];

		if(isset($_GET['Search']['kecamatan']))
			$search->kecamatan = $_GET['Search']['kecamatan'];

		if(isset($_GET['Search']['desa']))
			$search->desa = $_GET['Search']['desa'];


		if(User::isPerusahaan())
		{
			$search->pemilik = User::getIdPemilikByUserId();
		}


		$this->render('peta',array(
			'search'=>$search
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tower']))
		{
			$model->attributes=$_POST['Tower'];
			if($model->save()) {
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Tower');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Tower('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Tower']))
$model->attributes=$_GET['Tower'];

$this->render('admin',array(
'model'=>$model,
));
}

public function actionExport()
{
	$this->render('export');
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Tower::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='tower-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

	public function actionExportExcel()
	{

			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$criteria = new CDbCriteria;
			$params = array();

			if(!empty($_POST['id_operator'])) {
				$criteria->addCondition('id_operator=:operator');
				$params[':operator'] = $_POST['id_operator'];
			}

			if(!empty($_POST['id_pemilik'])) {
				$criteria->addCondition('id_pemilik=:pemilik');
				$params[':pemilik'] = $_POST['id_pemilik'];
			}

			if(!empty($_POST['id_penyewa'])) {
				$criteria->addCondition('id_penyewa=:penyewa');
				$params[':penyewa'] = $_POST['id_penyewa'];
			}

			if(!empty($_POST['id_kecamatan'])) {
				$criteria->addCondition('id_kecamatan=:kecamatan');
				$params[':kecamatan'] = $_POST['id_kecamatan'];
			}

			$criteria->params = $params;
			$criteria->order = 'id ASC';



			$PHPExcel = new PHPExcel();

			$PHPExcel->getActiveSheet()->getStyle('A3:M3')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getStyle("A1:L1")->getFont()->setSize(14);
			$PHPExcel->getActiveSheet()->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->mergeCells('A1:M1');//sama jLga
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "DATA TOWER");
		
			$PHPExcel->getActiveSheet()->setCellValue('A3', 'No');
			$PHPExcel->getActiveSheet()->setCellValue('B3', 'Operator');
			$PHPExcel->getActiveSheet()->setCellValue('C3', 'Pemilik');
			$PHPExcel->getActiveSheet()->setCellValue('D3', 'Penyewa');
			$PHPExcel->getActiveSheet()->setCellValue('E3', 'Alamat');
			$PHPExcel->getActiveSheet()->setCellValue('F3', 'Kecamatan');
			$PHPExcel->getActiveSheet()->setCellValue('G3', 'Luas Lahan');
			$PHPExcel->getActiveSheet()->setCellValue('H3', 'Koordinat');
			$PHPExcel->getActiveSheet()->setCellValue('I3', 'NJOP');
			$PHPExcel->getActiveSheet()->setCellValue('J3', 'Jumlah Operator');
			$PHPExcel->getActiveSheet()->setCellValue('K3', 'Jummlah Pengawasan');

				$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
				$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
				$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
				$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$PHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(22);
				$PHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);

			$i = 1;
			$kolom = 4;

			foreach(Tower::model()->findAll($criteria) as $data)
			{
				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, $data->getRelationField('Operator','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->getRelationField('Pemilik','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->getRelationField('Penyewa','nama'));
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->alamat);
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->getRelationField('Kecamatan','kecamatan'));
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->luas_lahan);
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->koordinat);
				$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->njop);
				$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->jumlah_operator);
				$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->jumlah_pengawasan);

				$PHPExcel->getActiveSheet()->getStyle('A3:M'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
				$PHPExcel->getActiveSheet()->getStyle('A2:M'.$kolom)->getFont()->setSize(9);
				$PHPExcel->getActiveSheet()->getStyle('A3:M'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	
									
				$i++; $kolom++;
			}

			$PHPExcel->getActiveSheet()->getStyle('A3:M'.$kolom)->getAlignment()->setWrapText(true);
			$PHPExcel->getActiveSheet()->getStyle('A3:M'.$kolom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
	
			$filename = time().'_Tower.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}




}
