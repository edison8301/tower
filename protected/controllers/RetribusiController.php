<?php

class RetribusiController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','cetak','createOto'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreateOto()
	{
		$model=new Retribusi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_GET['id_tower']))
			$model->id_tower = $_GET['id_tower'];

		if(isset($_POST['Retribusi']))
		{
			$model->attributes=$_POST['Retribusi'];
			if($model->validate())
			{
				$tower = Tower::model()->findByPk($model->id_tower);
				$perusahaan = Perusahaan::model()->findByPk($tower->id_pemilik);
			
				$model->id_perusahaan = $tower->id_operator;
				$model->nama_perusahaan = $perusahaan->nama;
				$model->alamat_perusahaan = $perusahaan->alamat;
				$model->peruntukan = 'Pembayaran Retribusi Pengendalian Menara Telekomunikasi';
				$model->lokasi = $tower->alamat;
				$model->luas_lahan = $tower->luas_lahan;
				$model->njop = $tower->njop;
				$model->persentase = 2;
				$model->jumlah_operator = $tower->jumlah_operator;
				$model->jumlah_pengawasan = $tower->jumlah_pengawasan;

				$model->setJatuhTempo();
				$model->tanggal_dibuat = date('Y-m-d');

				if($model->save())
					$this->redirect(array('view','id'=>$model->id));
			}
			
			
			
		}

		$this->render('createOto',array(
			'model'=>$model,
		));
	}
	
	public function actionCreate()
	{
		$model=new Retribusi;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Retribusi']))
		{
			$model->attributes=$_POST['Retribusi'];
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Retribusi']))
		{
			$model->attributes=$_POST['Retribusi'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Retribusi');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Retribusi('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Retribusi']))
			$model->attributes=$_GET['Retribusi'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Retribusi::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='retribusi-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionCetak($id)
	{
		$phpExcelPath = Yii::getPathOfAlias('ext.phpexcel');
	
		// Turn off our amazing library autoload 
		spl_autoload_unregister(array('YiiBase','autoload'));        
	
		//
		// making use of our reference, include the main class
		// when we do this, phpExcel has its own autoload registration
		// procedure (PHPExcel_Autoloader::Register();)
		include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
	   
		spl_autoload_register(array('YiiBase','autoload'));
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel = PHPExcel_IOFactory::load(Yii::app()->basePath.'/../exports/template/template.xlsx');
		$retribusi = Retribusi::model()->findByPk($id);
		$total = $retribusi->luas_lahan*$retribusi->njop*($retribusi->persentase/100)*$retribusi->jumlah_operator*$retribusi->jumlah_pengawasan;
		
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('D5', ': '.$retribusi->nama_perusahaan)
			->setCellValue('D6', ': '.$retribusi->alamat_perusahaan)
			->setCellValue('D7', ': '.$retribusi->peruntukan)
			->setCellValue('D8', ': '.$retribusi->lokasi)
			->setCellValue('D9', ': '.$retribusi->luas_lahan.' m2')
			->setCellValue('D10',': '.Bantu::tanggal($retribusi->jatuh_tempo))
			->setCellValue('D18', $retribusi->luas_lahan)
			->setCellValue('G18', $retribusi->njop)
			->setCellValue('H16', $retribusi->persentase.'%')
			->setCellValue('H18', $retribusi->persentase.'%')
			->setCellValue('J18', '3')
			->setCellValue('L18', '2')
			->setCellValue('D23', ucfirst(Bantu::getTerbilang($total,0).' rupiah'))
			->setCellValue('A3', 'Nomor : '.$retribusi->nomor);
						
		$objPHPExcel->getActiveSheet()->setTitle('Retribusi');
	
		$objPHPExcel->setActiveSheetIndex(0);
		
		ob_end_clean();
		ob_start();
	   
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="SKRD_'.$retribusi->nama_perusahaan.'.xlsx"');
		header('Cache-Control: max-age=0');
		$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		$objWriter->save('php://output');
	}
}